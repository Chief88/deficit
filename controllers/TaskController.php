<?php

namespace chief88\deficit\controllers;

use chief88\deficit\models\Task;
use chief88\deficit\models\TaskLog;
use yii\web\Controller;

class TaskController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionList()
    {
        $tasks = Task::find()->limit(30)->orderBy(['id' => SORT_DESC])->all();
        return $this->render('list', ['tasks' => $tasks]);
    }

    public function actionLog($type, $taskId)
    {
        $logs = TaskLog::find()->where(['task' => $taskId])->andWhere(['type' => $type])->orderBy(['id' => SORT_ASC])->all();
        return $this->render('logs', ['logs' => $logs]);
    }
}
