<?php

namespace chief88\deficit\tests\fixtures;

use yii\test\ActiveFixture;

class TaskPauseTypeFixture extends ActiveFixture {
    public $modelClass = 'chief88\deficit\models\TaskPauseType';
    public $dataFile = '@tests/../vendor/chief88/deficit/tests/fixtures/data/task_pause_type.php';
}