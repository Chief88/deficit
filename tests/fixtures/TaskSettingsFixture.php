<?php

namespace chief88\deficit\tests\fixtures;

use yii\test\ActiveFixture;

class TaskSettingsFixture extends ActiveFixture {
    public $modelClass = 'chief88\deficit\models\TaskSettings';
    public $dataFile = '@tests/../vendor/chief88/deficit/tests/fixtures/data/task_settings.php';
}