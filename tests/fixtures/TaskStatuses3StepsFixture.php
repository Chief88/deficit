<?php

namespace chief88\deficit\tests\fixtures;

use yii\test\ActiveFixture;

class TaskStatuses3StepsFixture extends ActiveFixture {
    public $modelClass = 'chief88\deficit\models\TaskStatus';
    public $dataFile = '@tests/../vendor/chief88/deficit/tests/fixtures/data/task3StepsStatuses.php';
}