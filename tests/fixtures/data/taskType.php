<?php

return [
    'task_type_1' => [
        'id' => 1,
        'slug' => 'test_type_1',
        'title' => 'Тестовая задача',
        'description' => 'Чисто для тестирования',
        'queue' => 'test_queue',
        'injection' => 'Injection',
        'persist_messages' => true,
        'log_group' => 'test_logs',
    ]

];
