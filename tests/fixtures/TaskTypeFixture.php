<?php

namespace chief88\deficit\tests\fixtures;

use yii\test\ActiveFixture;

class TaskTypeFixture extends ActiveFixture {
    public $modelClass = 'chief88\deficit\models\TaskType';
    public $dataFile = '@tests/../vendor/chief88/deficit/tests/fixtures/data/taskType.php';
}