<?php

namespace chief88\deficit\tests\fixtures;

use yii\test\ActiveFixture;

class TaskEmptyFixture extends ActiveFixture {
    public $modelClass = 'chief88\deficit\models\Task';
    public $dataFile = '@tests/../vendor/chief88/deficit/tests/fixtures/data/empty_task.php';
}