<?php
/**
 * Заглушка для использования при вызове методов в юнит-тестировании.
 */
namespace chief88\deficit\tests\injection;

use chief88\deficit\injections\DeficitInjection;
use yii\base\Component;

class Dummy extends Component implements DeficitInjection{

    public function handleErrors(){

    }

    public function buildLogData($data){
        return $data;
    }

    public function setInfoObject($infoObject){
    }
}