<?php

use AspectMock\Test as test;
use chief88\deficit\tests\fixtures\TaskTypeFixture;
use chief88\deficit\tests\fixtures\TaskStatuses3StepsFixture;
use yii\test\InitDbFixture;
use chief88\deficit\models\Task;
use chief88\deficit\models\TaskLog;
use chief88\deficit\models\TaskStatus;
use chief88\deficit\models\TaskType;
use chief88\deficit\tests\fixtures\TaskUnqueuedFixture;
use chief88\deficit\components\DeficitComponent;

class AMQOfflineContinueTaskTest extends \yii\codeception\DbTestCase
{
    protected function setUp()
    {
        Yii::$app->db->createCommand('truncate task, task_data, task_log, task_pause,
        task_pause_type, task_settings, task_status, task_type CASCADE;')->execute();

        parent::setUp();
        $this->loadFixtures();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->unloadFixtures();
        test::clean();
    }

    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'taskType' => TaskTypeFixture::className(),
            'taskStatuses' => TaskStatuses3StepsFixture::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function globalFixtures()
    {
        return [
            [
                'class' => InitDbFixture::className(),
                'schemas' => [
                    'deficit',
                ]
            ]
        ];
    }

    public function testContinueTask(){
        test::double('Yii', [
            'info' => null,
        ]);

        test::double('chief88\deficit\components\DeficitComponent', [
            'trySendToQueue' => null,
        ]);

        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $task = $deficit->createTask("test_type_1", "related_id", "test");
        $deficit->continueTask($task);

        /**
         * $status TaskStatus
         */
        $status = $task->taskStatus;

        if($status->id === 2){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }

}