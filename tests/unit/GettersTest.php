<?php

use AspectMock\Test as test;
use chief88\deficit\tests\fixtures\TaskTypeFixture;
use chief88\deficit\tests\fixtures\TaskStatuses3StepsFixture;
use yii\test\InitDbFixture;
use chief88\deficit\models\Task;
use chief88\deficit\models\TaskLog;
use chief88\deficit\models\TaskStatus;
use chief88\deficit\models\TaskType;
use chief88\deficit\tests\fixtures\TaskUnqueuedFixture;
use chief88\deficit\tests\fixtures\TaskFixture;
use chief88\deficit\tests\fixtures\TaskSettingsFixture;
use chief88\deficit\components\DeficitComponent;
use chief88\deficit\exceptions\StatusNotFoundException;
use chief88\deficit\exceptions\NoTaskException;
use chief88\deficit\exceptions\NoInjectionException;
use chief88\deficit\tests\fixtures\TaskPauseTypeFixture;
use chief88\deficit\exceptions\NoTaskPauseTypeException;
use chief88\deficit\exceptions\NoTaskTypeException;
use chief88\deficit\exceptions\NoSuchSettingException;
use chief88\deficit\models\TaskPause;

class GettersTest extends \yii\codeception\DbTestCase
{
    protected function setUp()
    {
        Yii::$app->db->createCommand('truncate task, task_data, task_log, task_pause,
        task_pause_type, task_settings, task_status, task_type CASCADE;')->execute();

        parent::setUp();
        $this->loadFixtures();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->unloadFixtures();
        test::clean();
    }

    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'taskType' => TaskTypeFixture::className(),
            'taskStatuses' => TaskStatuses3StepsFixture::className(),
            'task' => TaskFixture::className(),
            'taskPauseType' => TaskPauseTypeFixture::className(),
            'taskSettings' => TaskSettingsFixture::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function globalFixtures()
    {
        return [
            [
                'class' => InitDbFixture::className(),
                'schemas' => [
                    'deficit',
                ]
            ]
        ];
    }

    public function testIsInited(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $this->assertFalse($deficit->isInited());
        $task = Task::findOne(1);
        $deficit->setTask($task);
        $this->assertTrue($deficit->isInited());
    }

    public function testGetFirstStatus(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $taskType = TaskType::findOne(1);
        $status = $deficit->getFirstStatus($taskType);

        $this->assertTrue($status->is_new);
    }

    public function testGetNextStatus(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $taskStatus = TaskStatus::findOne(1);
        $status = $deficit->getNextStatus($taskStatus);

        if($status->id == 2){
            $this->assertTrue(true);
        }

        $status = $deficit->getNextStatus($status);

        if($status->id == 3){
            $this->assertTrue(true);
        }

        $status = $deficit->getNextStatus($status);

        if($status->id == 4){
            $this->assertTrue(true);
        }

        /*
         * Ну и протестируем эксепшоны, получим следующий статус для успешного статуса
         */
        $taskStatus = TaskStatus::findOne(11);
        try{
            $status = $deficit->getNextStatus($taskStatus);
        }catch (StatusNotFoundException $e){
            $this->assertTrue(true);
        }

        /*
         * Ну и протестируем эксепшоны, получим следующий статус для статуса ошибка
         */
        $taskStatus = TaskStatus::findOne(12);
        try{
            $status = $deficit->getNextStatus($taskStatus);
        }catch (StatusNotFoundException $e){
            $this->assertTrue(true);
        }
    }

    public function testGetPreviousStatus()
    {
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $taskStatus = TaskStatus::findOne(11);

        $status = $deficit->getPreviousStatus($taskStatus);
        if($status->id == 10){
            $this->assertTrue(true);
        }

        $status = $deficit->getPreviousStatus($status);
        if($status->id == 9){
            $this->assertTrue(true);
        }

        $status = $deficit->getPreviousStatus($status);
        if($status->id == 8){
            $this->assertTrue(true);
        }

        $taskStatus = TaskStatus::findOne(1);
        try{
            $status = $deficit->getPreviousStatus($taskStatus);
        }catch (StatusNotFoundException $e){
            $this->assertTrue(true);
        }

        $taskStatus = TaskStatus::findOne(12);
        try{
            $status = $deficit->getPreviousStatus($taskStatus);
        }catch (StatusNotFoundException $e){
            $this->assertTrue(true);
        }
    }

    public function testGetErrorStatus()
    {
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        $taskType = TaskType::findOne(['id' => 1]);
        $reflection = new \ReflectionClass(get_class($deficit));
        $method = $reflection->getMethod("getErrorStatus");
        $method->setAccessible(true);
        $errorStatus = $method->invokeArgs($deficit, [$taskType]);
        if($errorStatus->id == 12 && $errorStatus->is_error = true){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }

    public function testGetSuccessStatus(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        $taskType = TaskType::findOne(['id' => 1]);
        $reflection = new \ReflectionClass(get_class($deficit));
        $method = $reflection->getMethod("getSuccessStatus");
        $method->setAccessible(true);
        $successStatus = $method->invokeArgs($deficit, [$taskType]);
        if($successStatus->id == 11 && $successStatus->is_success = true){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }


    public function testSetNextStatus(){
        test::double('Yii', ['info' => null,]);
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        $task = Task::findOne(['id' => 1]);
        $deficit->setTask($task);
        $status = $task->taskStatus;
        if($status->id == 1){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }

        $deficit->setNextStatus($task);
        $task = Task::findOne(['id' => 1]);
        $status = $task->taskStatus;
        if($status->id == 2){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }


    public function testSetPreviousStatus(){
        test::double('Yii', ['info' => null,]);
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        /** @var Task $task */
        $task = Task::findOne(['id' => 1]);
        $deficit->setTask($task);
        $task->status = 11;
        $task->save();

        $deficit->setPreviousStatus($task);
        $task = Task::findOne(['id' => 1]);
        $status = $task->taskStatus;
        if($status->id == 10){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }

        $deficit->setPreviousStatus($task);
        $task = Task::findOne(['id' => 1]);
        $status = $task->taskStatus;
        if($status->id == 9){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }

    public function testTask2Error()
    {
        test::double('Yii', ['info' => null,]);
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        /** @var Task $task */
        $task = Task::findOne(['id' => 1]);
        $deficit->setTask($task);
        $task->status = 1;
        $deficit->task2Error($task);
        $task = Task::findOne(['id' => 1]);
        if($task->taskStatus->is_error === true){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }

    public function testTask2Success()
    {
        test::double('Yii', ['info' => null,]);
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        /** @var Task $task */
        $task = Task::findOne(['id' => 1]);
        $deficit->setTask($task);
        $task->status = 1;
        $deficit->task2Success($task);
        $task = Task::findOne(['id' => 1]);
        if($task->taskStatus->is_success === true){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }

    public function testGetTask()
    {
        test::double('Yii', ['info' => null,]);
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        try{
            $deficit->getTask();
        }catch (NoTaskException $e){
            $this->assertTrue(true);
        }

        /** @var Task $task */
        $task = Task::findOne(['id' => 1]);
        $deficit->setTask($task);

        $task = $deficit->getTask();

        if($task->status == 1){
            $this->assertTrue(true);
        }
    }

    public function testInjectLogic()
    {
        test::double('Yii', ['info' => null,]);
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        /** @var TaskType $taskType */
        $taskType = TaskType::findOne(1);

        $taskType->injection = null;
        try{
            $deficit->injectLogic($taskType);
        }catch (NoInjectionException $e){
            $this->assertTrue(true);
        }

        /** @var TaskType $taskType */
        $taskType = TaskType::findOne(1);
        $deficit->injectLogic($taskType);
        $this->assertTrue(get_class($deficit->getInjection()) == "chief88\\deficit\\tests\\injection\\Dummy");

    }


    public function testGetInjection()
    {
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        try{
            $deficit->getInjection();
        }catch (NoInjectionException $e){
            $this->assertTrue(true);
        }
    }

    public function testGetPauseType(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $pauseType = $deficit->getPauseType("no_money", "test_type_1");
        $this->assertTrue($pauseType->slug == 'no_money');

        try{
            $pauseType = $deficit->getPauseType("no_money_123", "test_type_1");
        }catch (NoTaskPauseTypeException $e){
            $this->assertTrue(true);
        }
    }

    public function testGetTaskType(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $pauseType = $deficit->findTaskType("test_type_1");

        $this->assertTrue($pauseType->id == 1 && $pauseType->slug == "test_type_1");

        try{
            $pauseType = $deficit->findTaskType("test_type_123");
        }catch (NoTaskTypeException $e){
            $this->assertTrue(true);
        }
    }

    public function testGetTaskByRelated(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;


        $task = $deficit->getTaskByRelated('test_type_1', '1234');
        $this->assertTrue($task->id == 1);
    }

    public function testGetSetting(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        try{
            $deficit->getSetting("some_setting");
        }catch (NoSuchSettingException $e){
            $this->assertTrue(true);
        }
    }

    public function testPauseTask(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        /** @var Task $task */
        $task = Task::findOne(1);
        $task->status = 3;
        $task->save();

        try{
            $deficit->pauseTask('no_money');
        }catch (NoTaskException $e){
            $this->assertTrue(true);
        }

        $deficit->setTask($task);

        $pauses = TaskPause::findAll(['task' => 1]);
        $this->assertTrue(sizeof($pauses) == 0);

        $deficit->pauseTask('no_money');

        $pauses = TaskPause::findAll(['task' => 1]);
        $this->assertTrue(sizeof($pauses) == 1);
    }

    public function testResumeOneTask(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        /** @var Task $task */
        $task = Task::findOne(1);
        $task->status = 3;
        $task->save();

        $deficit->setTask($task);

        $deficit->pauseTask('no_money');
        $pauses = TaskPause::findAll(['is_active' => true]);
        $this->assertTrue(sizeof($pauses) == 1);

        $deficit->resumeOneTask($pauses[0]);
        $pauses = TaskPause::findAll(['is_active' => true]);
        $this->assertTrue(sizeof($pauses) == 0);
    }

    public function testResumeTasks(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        /** @var Task $task */
        $task = Task::findOne(1);
        $task->status = 3;
        $task->save();

        $deficit->setTask($task);

        $deficit->pauseTask('no_money');
        $pauses = TaskPause::findAll(['is_active' => true]);
        $this->assertTrue(sizeof($pauses) == 1);

        $deficit->resumeTasks('no_money', 'test_type_1');
        $pauses = TaskPause::findAll(['is_active' => true]);
        $this->assertTrue(sizeof($pauses) == 0);
    }

    public function testSetSettings(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        /** @var TaskType $taskType */
        $taskType = TaskType::findOne(1);
        $deficit->setSettings($taskType);

        $this->assertTrue($deficit->getSetting("slug_1") == "value_1");
        $this->assertTrue($deficit->getSetting("slug_2") == "value_2");
    }

}