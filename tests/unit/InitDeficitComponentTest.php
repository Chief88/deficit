<?php

use chief88\deficit\exceptions\NoTaskTypeException;
use yii\test\InitDbFixture;
use chief88\deficit\components\DeficitComponent;

class InitDeficitComponentTest extends \yii\codeception\DbTestCase
{
    protected function setUp()
    {
        Yii::$app->db->createCommand('truncate task, task_data, task_log, task_pause,
        task_pause_type, task_settings, task_status, task_type CASCADE;')->execute();

        parent::setUp();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function globalFixtures()
    {
        return [
            [
                'class' => InitDbFixture::className(),
                'schemas' => [
                    'deficit',
                ]
            ]
        ];
    }

    public function testInit(){
        try{
            /** @var DeficitComponent $deficit */
            $deficit = \Yii::$app->deficitComponent;
        }catch (NoTaskTypeException $e){
            $this->assertTrue(true);
        }

    }

}