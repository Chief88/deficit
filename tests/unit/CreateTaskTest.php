<?php

use AspectMock\Test as test;
use chief88\deficit\tests\fixtures\TaskTypeFixture;
use chief88\deficit\tests\fixtures\TaskStatuses3StepsFixture;
use yii\test\InitDbFixture;
use chief88\deficit\components\DeficitComponent;

class CreateTaskTest extends \yii\codeception\DbTestCase
{
    protected function setUp()
    {
        Yii::$app->db->createCommand('truncate task, task_data, task_log, task_pause,
        task_pause_type, task_settings, task_status, task_type CASCADE;')->execute();

        parent::setUp();
        $this->loadFixtures();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->unloadFixtures();
        test::clean();
    }

    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'taskType' => TaskTypeFixture::className(),
            'taskStatuses' => TaskStatuses3StepsFixture::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function globalFixtures()
    {
        return [
            [
                'class' => InitDbFixture::className(),
                'schemas' => [
                    'deficit',
                ]
            ]
        ];
    }

    /*
     * Проверяем, что после создания задачи:
     * 1) Задача создаётся и возвращается методом createTask()
     * 2) Задача создаётся и сохраняется в базе данных
     * 3) После создания задачи она имеет статус "новая"
     */
    public function testCreateTask(){
        test::double('Yii', [
            'info' => null,
        ]);

        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $task = $deficit->createTask("test_type_1", "related_id", "test");

        /**
         * $status TaskStatus
         */
        $status = $task->taskStatus;

        if($status->id === 1){
            $this->assertTrue(true);
        }else{
            $this->assertTrue(false);
        }
    }
}