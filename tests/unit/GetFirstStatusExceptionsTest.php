<?php

use AspectMock\Test as test;
use chief88\deficit\tests\fixtures\TaskTypeFixture;
use chief88\deficit\tests\fixtures\TaskStatuses3StepsFixture;
use yii\test\InitDbFixture;
use chief88\deficit\models\Task;
use chief88\deficit\models\TaskLog;
use chief88\deficit\models\TaskStatus;
use chief88\deficit\models\TaskType;
use chief88\deficit\tests\fixtures\TaskUnqueuedFixture;
use chief88\deficit\tests\fixtures\TaskFixture;
use chief88\deficit\components\DeficitComponent;
use chief88\deficit\exceptions\StatusNotFoundException;

class GetFirstStatusExceptionsTest extends \yii\codeception\DbTestCase
{
    protected function setUp()
    {
        Yii::$app->db->createCommand('truncate task, task_data, task_log, task_pause,
        task_pause_type, task_settings, task_status, task_type CASCADE;')->execute();

        parent::setUp();
        $this->loadFixtures();
    }

    protected function tearDown()
    {
        parent::tearDown();
        $this->unloadFixtures();
        test::clean();
    }

    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'taskType' => TaskTypeFixture::className(),
            'task' => TaskFixture::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function globalFixtures()
    {
        return [
            [
                'class' => InitDbFixture::className(),
                'schemas' => [
                    'deficit',
                ]
            ]
        ];
    }

    public function testGetFirstStatusException(){
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $taskType = TaskType::findOne(1);

        try{
            $status = $deficit->getFirstStatus($taskType);
        }catch (StatusNotFoundException $e){
            $this->assertTrue(true);
        }
    }

}