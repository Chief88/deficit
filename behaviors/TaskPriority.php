<?php

namespace chief88\deficit\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class TaskPriority extends Behavior
{

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'setDefaultPriority',
        ];
    }

    public function setDefaultPriority($event)
    {
        $this->owner->priority = 4;
    }
}
