<?php

namespace chief88\deficit\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class CreatedAt extends Behavior
{

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'setCreatedAt',
        ];
    }

    public function setCreatedAt($event)
    {
        /** @var \DateTime $now */
        $now = new \DateTime('now');
        $this->owner->created = $now->format("Y-m-d H:i:s");
    }
}
