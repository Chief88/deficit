<?php

namespace chief88\deficit\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class UpdatedAt extends Behavior
{

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'setUpdatedAt',
        ];
    }

    public function setUpdatedAt($event)
    {
        /** @var \DateTime $now */
        $now = new \DateTime('now');
        $this->owner->updated = $now->format("Y-m-d H:i:s");
    }
}
