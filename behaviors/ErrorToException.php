<?php

namespace chief88\deficit\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Поведение для AR-моделей, которое выбросит исключение с текстом первой ошибки валидации.
 * Т.е. при сохранении модели нет необходимости проверять на наличие ошибок, поведение сделает это само.
 * Поведение не требует указания дополнительных параметров при подключении.
 *
 * Class ErrorToException
 * @package app\behaviors
 *
 * @property ActiveRecord $owner Объект модели, к которой прикреплено данное поведение
 */
class ErrorToException extends Behavior {

    public $exception = null;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_VALIDATE => 'errorToException',
        ];
    }

    /**
     * Если при валидации модели возникли ошибки, то выбросить исключение с текстом первой из них.
     * @param $event - Объект события
     * @throws \Exception Если при при валидации модели возникли ошибки
     */
    public function errorToException($event)
    {
        if(isset($this->exception)){
            /** @var \Exception $exception */
            $exception = $this->exception;
            throw $exception;
        }else{
            $errors = $this->owner->getFirstErrors();
            if (!empty($errors)) {
                throw new \Exception(current($errors));
            }
        }
    }
}
