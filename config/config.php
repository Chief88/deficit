<?php

$config['modules']['deficit'] = [
    'class' => 'chief88\deficit\Deficit',
    'params' => [
        'root_dir' => 'dummy_root_dir',
    ]
];

$config['components']['deficitComponent'] = [
    'class' => 'chief88\deficit\components\DeficitComponent',
    'default_support_email' => 'serg.latyshkov@gmail.com',
];

$config['components']['deficitQueue'] = [
    'class' => 'chief88\deficit\components\Queue',
    'brokers' => [
        'default' => [
            'url' => 'tcp://127.0.0.1:61613',
            'login' => 'admin',
            'pass' => 'pass',
        ],
    ],
];

$config['components']['log']['targets'][] = [
    'class' => 'chief88\deficit\logTargets\ActiveMQTarget',
    'levels' => ['error', 'warning', 'info'],
    'categories' => ['deficit_category'],
    'logVars' => [],
];

$config['bootstrap'][] = 'deficit';
$config['bootstrap'][] = 'log';
