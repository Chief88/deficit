<?php
use \chief88\deficit\models\TaskStatus;
use \chief88\deficit\models\TaskType;
?>
<div class="activemq-default-index">
    <h1>Логи задачи</h1>

        <table class="table table-striped">
            <th>ID</th>
            <th>Действие</th>
            <th>Время</th>

            <?php foreach($logs as $key=>$log): ?>
            <tr>
                <td><?= $log->id; ?></td>
                <td><?= $log->message; ?></td>
                <td><?= $log->created; ?></td>
            </tr>
            <?php endforeach; ?>
        </table>


</div>
