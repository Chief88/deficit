<?php
use \chief88\deficit\models\TaskStatus;
use \chief88\deficit\models\TaskType;
?>
<div class="activemq-default-index">
    <h1>Список задач</h1>

        <table class="table table-striped">
            <tr>
                <th>ID</th>
                <th>Задача</th>
                <th>Статус</th>
                <th>Описание статуса</th>
                <th>Логи</th>
            </tr>
            <?php foreach($tasks as $key=>$task): ?>
            <tr>
                <td><?= $task->id; ?></td>
                <?php $type = TaskType::findOne($task->type); ?>
                <td><?= $type->description ?></td>
                <?php $status = TaskStatus::findOne($task->status); ?>
                <td><?= $status->title; ?></td>
                <td><?= $status->description; ?></td>
                <td>
                    <a href="/deficit/task/log/?type=info&taskId=<?= $task->id; ?>" class="btn btn-success">Info</a>
                    <a href="/deficit/task/log/?type=detail&taskId=<?= $task->id; ?>" class="btn btn-warning">Detail</a>
                    <a href="/deficit/task/log/?type=debug&taskId=<?= $task->id; ?>" class="btn btn-danger">Debug</a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>


</div>
