<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_103842_alter_log_string_to_text extends Migration
{
    public function up()
    {
        $this->alterColumn("task_log", "message", "text");
    }

    public function down()
    {
        $this->alterColumn("task_log", "message", "string");
    }
}
