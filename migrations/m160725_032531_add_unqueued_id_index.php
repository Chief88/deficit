<?php

use yii\db\Migration;

class m160725_032531_add_unqueued_id_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('task_unqueue_id_index', 'task', ['id', 'is_unqueued']);
    }

    public function safeDown()
    {
        $this->dropIndex('task_unqueue_id_index', 'task');
    }
}
