<?php

use yii\db\Schema;
use yii\db\Migration;

class m151202_102836_add_task_repeat_counter extends Migration
{
    public function up()
    {
        $this->addColumn("task", "repeat_counter", "integer");
    }

    public function down()
    {
        $this->dropColumn("task", "repeat_counter");
    }
}
