<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_094347_add_is_success_status_field extends Migration
{
    public function up()
    {
        $this->addColumn("task_status", "is_success", "boolean");
    }

    public function down()
    {
        $this->dropColumn("task_status", "is_success");
    }
}
