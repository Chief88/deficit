<?php

use yii\db\Migration;

class m160714_135324_add extends Migration
{
    public function safeUp()
    {
        $this->createIndex('task_updated_idx', 'task', 'updated');
    }

    public function safeDown()
    {
        $this->dropIndex('task_updated_idx', 'task');
    }
}
