<?php

use yii\db\Schema;
use yii\db\Migration;

class m151006_103759_add_value_field_to_settings extends Migration
{
    public function up()
    {
        $this->addColumn("task_settings", "value", "string");
    }

    public function down()
    {
        $this->dropColumn("task_settings", "value");
    }
}
