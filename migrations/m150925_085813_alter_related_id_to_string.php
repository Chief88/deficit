<?php

use yii\db\Schema;
use yii\db\Migration;

class m150925_085813_alter_related_id_to_string extends Migration
{
    public function up()
    {
        $this->alterColumn("task", "related", "string");
    }

    public function down()
    {
        $this->alterColumn("task", "related", "integer");
    }
}
