<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_031752_remove_settings_slug_constraint extends Migration
{
    public function up()
    {
        $this->dropIndex('task_settings_idx', 'task_settings');
    }

    public function down()
    {
        $this->createIndex('task_settings_idx', 'task_settings', 'slug', true);
    }
}
