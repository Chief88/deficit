<?php

use yii\db\Migration;

class m151019_050042_drop_pause_type_slug_constraint extends Migration
{
    public function up()
    {
        $this->dropIndex('task_pause_type_idx', 'task_pause_type');
    }

    public function down()
    {
        $this->createIndex('task_pause_type_idx', 'task_pause_type', 'slug', true);
    }
}
