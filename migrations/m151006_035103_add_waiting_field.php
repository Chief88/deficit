<?php

use yii\db\Schema;
use yii\db\Migration;

class m151006_035103_add_waiting_field extends Migration
{
    public function up()
    {
        $this->addColumn("task_status", "waiting", "integer");
    }

    public function down()
    {
        $this->dropColumn("task_status", "waiting");
    }
}
