<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_062959_add_task_pause_tables extends Migration
{
    public function up()
    {
        $this->createTable('task_pause_type', [
            'id' => Schema::TYPE_PK,
            'task_type' => Schema::TYPE_INTEGER,
            'slug' => Schema::TYPE_STRING,
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'is_resume_all_tasks' => Schema::TYPE_BOOLEAN,
            'created' => Schema::TYPE_DATETIME,
        ]);
        $this->addForeignKey("pause_type_fk", "task_pause_type", "task_type", "task_type", "id");
        $this->createIndex('task_pause_type_idx', 'task_pause_type', 'slug', true);

        $this->createTable('task_pause', [
            'id' => Schema::TYPE_PK,
            'task' => Schema::TYPE_INTEGER,
            'pause_type' => Schema::TYPE_INTEGER,
            'is_active' => Schema::TYPE_BOOLEAN,
            'created' => Schema::TYPE_DATETIME,
            'updated' => Schema::TYPE_DATETIME,
        ]);
        $this->addForeignKey("pause_fk", "task_pause", "pause_type", "task_pause_type", "id");
    }

    public function down()
    {
        $this->dropTable('task_pause');
        $this->dropTable('task_pause_type');
    }
}
