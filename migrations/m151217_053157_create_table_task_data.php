<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_053157_create_table_task_data extends Migration
{
    public function safeUp()
    {
        $this->createTable('task_data', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->notNull(),
            'data' => 'JSON NOT NULL',
        ]);
        $this->addForeignKey('task_id_fk', 'task_data', 'task_id', 'task', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('task_data');
    }
}
