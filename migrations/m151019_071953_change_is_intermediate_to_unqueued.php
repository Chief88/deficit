<?php

use yii\db\Schema;
use yii\db\Migration;

class m151019_071953_change_is_intermediate_to_unqueued extends Migration
{
    public function up()
    {
        $this->renameColumn("task_status", "is_intermediate", "is_unqueued");
    }

    public function down()
    {
        $this->renameColumn("task_status", "is_unqueued", "is_intermediate");
    }
}
