<?php

use yii\db\Migration;

class m160723_041603_add_denormalized_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('task', 'is_unqueued', \yii\db\pgsql\Schema::TYPE_BOOLEAN);
        $this->createIndex('deficit_task_unqueued', 'task', 'is_unqueued');

        $this->addColumn('task', 'is_success', \yii\db\pgsql\Schema::TYPE_BOOLEAN);
        $this->createIndex('deficit_task_success', 'task', 'is_success');

        $this->addColumn('task', 'is_error', \yii\db\pgsql\Schema::TYPE_BOOLEAN);
        $this->createIndex('deficit_task_error', 'task', 'is_error');
    }

    public function safeDown()
    {
        $this->dropIndex('deficit_task_unqueued', 'task');
        $this->dropIndex('deficit_task_success', 'task');
        $this->dropIndex('deficit_task_error', 'task');

        $this->dropColumn('task', 'is_unqueued');
        $this->dropColumn('task', 'is_success');
        $this->dropColumn('task', 'is_error');
    }
}
