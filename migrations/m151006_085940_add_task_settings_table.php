<?php

use yii\db\Schema;
use yii\db\Migration;

class m151006_085940_add_task_settings_table extends Migration
{
    public function up()
    {
        $this->createTable('task_settings', [
            'id' => Schema::TYPE_PK,
            'task_type' => Schema::TYPE_INTEGER,
            'slug' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'created' => Schema::TYPE_DATETIME,
        ]);
        $this->addForeignKey("settings_fk", "task_settings", "task_type", "task_type", "id");
        $this->createIndex('task_settings_idx', 'task_settings', 'slug', true);
    }

    public function down()
    {
        echo "m151006_085940_add_task_settings_table cannot be reverted.\n";

        return false;
    }
}
