<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_040646_alter_table_task_set_default_repeat_counter extends Migration
{

    public function safeUp()
    {
        $this->update('task', ['repeat_counter' => 0], 'repeat_counter IS NULL');
        $this->alterColumn('task', 'repeat_counter', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        //откатывать не нужно
    }
}
