<?php

use yii\db\Schema;
use yii\db\Migration;

class m151013_062638_add_injection_field_to_type extends Migration
{
    public function up()
    {
        $this->addColumn("task_type", "injection", "string");
    }

    public function down()
    {
        $this->dropColumn("task_type", "injection");
    }
}
