<?php

use yii\db\Migration;

/**
 * Handles adding injection_field to table `status`.
 */
class m160530_062358_add_injection_field_to_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('task_status', 'injection', \yii\db\pgsql\Schema::TYPE_STRING);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('task_status', 'injection');
    }
}
