<?php

use yii\db\Migration;

class m151129_085201_add_task_type_field_log_group extends Migration
{
    public function up()
    {
        $this->addColumn("task_type", "log_group", "string");
    }

    public function down()
    {
        $this->dropColumn("task_type", "log_group");
    }
}
