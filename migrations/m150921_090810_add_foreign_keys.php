<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_090810_add_foreign_keys extends Migration
{
    public function up()
    {
        $this->addForeignKey("type_fk", "task", "type", "task_type", "id");
        $this->addForeignKey("status_fk", "task_status", "task_type", "task_type", "id");

        $this->addForeignKey("status_next_fk", "task_status", "next", "task_status", "id");
        $this->addForeignKey("status_previous_fk", "task_status", "previous", "task_status", "id");
    }

    public function down()
    {
        echo "m150921_090810_add_foreign_keys cannot be reverted.\n";

        return false;
    }
}
