<?php

use yii\db\Migration;

class m161004_074109_add_column_parent_id_in_table_task extends Migration
{
    public function safeUp()
    {
        $this->addColumn('task', 'parent_id', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('task', 'parent_id');
    }
}
