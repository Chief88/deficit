<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_072717_create_tables extends Migration
{
    public function up()
    {
        $this->createTable('task', [
            'id' => Schema::TYPE_PK,
            'status' => Schema::TYPE_INTEGER,
            'type' => Schema::TYPE_INTEGER,
            'related' => Schema::TYPE_INTEGER,
            'description' => Schema::TYPE_TEXT,
            'created' => Schema::TYPE_DATETIME,
            'updated' => Schema::TYPE_DATETIME,
        ]);

        $this->createTable('task_type', [
            'id' => Schema::TYPE_PK,
            'slug' => Schema::TYPE_STRING,
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'queue' => Schema::TYPE_STRING,
            'created' => Schema::TYPE_DATETIME,
        ]);
        $this->createIndex('task_type_idx', 'task_type', 'slug', true);

        $this->createTable('task_status', [
            'id' => Schema::TYPE_PK,
            'task_type' => Schema::TYPE_INTEGER,
            'slug' => Schema::TYPE_STRING,
            'command' => Schema::TYPE_STRING,
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'next' => Schema::TYPE_INTEGER,
            'previous' => Schema::TYPE_INTEGER,
            'created' => Schema::TYPE_DATETIME,
            'is_new' => Schema::TYPE_BOOLEAN,
            'is_error' => Schema::TYPE_BOOLEAN,
        ]);

        $this->createTable('task_log', [
            'id' => Schema::TYPE_PK,
            'task' => Schema::TYPE_INTEGER,
            'message' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_STRING,
            'created' => Schema::TYPE_DATETIME,
        ]);

    }

    public function down()
    {
        $this->dropTable('task');
        $this->dropTable('task_type');
        $this->dropTable('task_status');
        $this->dropTable('task_log');
    }

}
