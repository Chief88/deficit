<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_042611_add_priority_field_to_task extends Migration
{
    public function up()
    {
        $this->addColumn("task", "priority", "integer");
    }

    public function down()
    {
        $this->dropColumn("task", "priority");
    }
}
