<?php

use yii\db\Migration;

/**
 * Handles adding broker to table `task_type`.
 */
class m160711_083711_add_broker_to_task_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('task_type', 'broker_name', 'JSON');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('task_type', 'broker_name');
    }
}
