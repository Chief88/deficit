<?php

use yii\db\Schema;
use yii\db\Migration;

class m150925_072124_add_is_intermediate_column extends Migration
{
    public function up()
    {
        $this->addColumn("task_status", "is_intermediate", "boolean");
    }

    public function down()
    {
        $this->dropColumn("task_status", "is_intermediate");
    }
}
