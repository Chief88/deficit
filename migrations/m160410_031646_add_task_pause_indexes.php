<?php

use yii\db\Migration;

class m160410_031646_add_task_pause_indexes extends Migration
{
    public function up()
    {
        $this->createIndex('pause_active_idx', 'task_pause', 'is_active');
        $this->createIndex('pause_task_id_idx', 'task_pause', 'task');

    }

    public function down()
    {
        $this->dropIndex('pause_active_idx', 'task_pause');
        $this->dropIndex('pause_task_id_idx', 'task_pause');
    }
}
