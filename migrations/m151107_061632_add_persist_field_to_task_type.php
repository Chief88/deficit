<?php

use yii\db\Schema;
use yii\db\Migration;

class m151107_061632_add_persist_field_to_task_type extends Migration
{
    public function up()
    {
        $this->addColumn("task_type", "persist_messages", "boolean");
    }

    public function down()
    {
        $this->dropColumn("task_type", "persist_messages");
    }
}
