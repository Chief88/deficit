<?php

use yii\db\Schema;
use yii\db\Migration;

class m151229_051652_add_log_level_field extends Migration
{
    public function up()
    {
        $this->addColumn("task_log", "level", "string");
    }

    public function down()
    {
        $this->dropColumn("task_log", "level");
    }
}
