<?php

namespace chief88\deficit\services;

class CentraldesktopLogger implements \Psr\Log\LoggerInterface
{
    public function emergency($message, array $context = array())
    {
        // TODO: Implement emergency() method.
    }

    public function alert($message, array $context = array())
    {
        // TODO: Implement alert() method.
    }

    public function critical($message, array $context = array())
    {
        // TODO: Implement critical() method.
    }

    public function error($message, array $context = array())
    {
        // TODO: Implement error() method.
    }

    public function warning($message, array $context = array())
    {
        // TODO: Implement warning() method.
    }

    public function notice($message, array $context = array())
    {
        // TODO: Implement notice() method.
    }

    public function info($message, array $context = array())
    {
        // TODO: Implement info() method.
    }

    public function debug($message, array $context = array())
    {
        if ($message == 'Sending message') return;
        \Yii::info($message, 'amq_debug');
        if (!empty($context)) {
            \Yii::info('begin context: ', 'amq_debug');
            foreach ($context as $item => $value) {
                \Yii::info("$item: $value", 'amq_debug');
            }
            \Yii::info('end context', 'amq_debug');
        }
    }

    public function log($level, $message, array $context = array())
    {
        // TODO: Implement log() method.
    }
}
