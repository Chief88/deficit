<?php

namespace chief88\deficit\models;

use chief88\deficit\exceptions\NoRelatedException;
use chief88\deficit\exceptions\StatusMismatchException;
use chief88\deficit\exceptions\TaskAlreadySuccessfulException;
use chief88\deficit\exceptions\TaskPausedException;
use chief88\deficit\components\DeficitComponent as deficit;
use chief88\deficit\services\Cache;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;

/**
 * Модель задачи для activemq
 *
 * @property integer $id
 * @property integer $status
 * @property integer $type
 * @property string $related
 * @property string $description
 * @property string $created
 * @property string $updated
 * @property integer $priority
 * @property integer $repeat_counter
 * @property boolean $is_unqueued
 * @property boolean $is_success
 * @property boolean $is_error
 * @property integer $parent_id ID родительской задачи
 *
 * @property TaskPause[] $taskPauses Массив объектов пауз данной задачи
 * @property TaskStatus $taskStatus Объект статуса задачи
 * @property TaskType $taskType Объект типа задачи
 * @property TaskData $taskData Объект данных задачи
 * @property Task[] $childrenTasks Массив объектов дочерних задач
 * @property Task $parenTask Объект родительской задачи
 * @property Task $ancestorTask Объект задачи-предка (самый первый предок)
 */
class Task extends ActiveRecord
{

    public $commandName;

    /*
     * Сценарий ответственного начала работы, с проверкой этапа
     */
    const SCENARIO_BEGIN_WORKING = 'begin_working';
    /*
     * Сценарий безответственного начала работы, не платёж, не деньги, ну и ладно
     */
    const SCENARIO_EASY_WORKING = 'easy_working';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'createdAt' => [
                'class' => 'chief88\deficit\behaviors\CreatedAt',
            ],
            'updatedAt' => [
                'class' => 'chief88\deficit\behaviors\UpdatedAt',
            ],
            'priority' => [
                'class' => 'chief88\deficit\behaviors\TaskPriority',
            ],
            'errorToException' => [
                'class' => 'chief88\deficit\behaviors\ErrorToException',
            ],
        ];
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status'], 'validateStatusNotSuccess', 'on' => [
                self::SCENARIO_BEGIN_WORKING,
                self::SCENARIO_EASY_WORKING,
            ]],
            [['status'], 'validateStatusNotFirst', 'on' => [
                self::SCENARIO_BEGIN_WORKING,
                self::SCENARIO_EASY_WORKING,
            ]],
            [['status'], 'validateStatusNotError', 'on' => [
                self::SCENARIO_BEGIN_WORKING,
                self::SCENARIO_EASY_WORKING,
            ]],
            [['status'], 'validateStatusByCmd', 'on' => [
                self::SCENARIO_BEGIN_WORKING
            ]],
            [['taskPauses'], 'validateTaskActive', 'on' => [
                self::SCENARIO_BEGIN_WORKING,
                self::SCENARIO_EASY_WORKING,
            ]],
            [['related'], 'validateRelated', 'on' => [
                self::SCENARIO_BEGIN_WORKING,
                self::SCENARIO_EASY_WORKING,
            ], 'skipOnEmpty' => false],
        ]);

    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_BEGIN_WORKING => ['status', 'related', 'taskPauses']
        ]);
    }

    /**
     * Отношение для пауз данной задачи
     * Паузы мы не кэшируем, чтобы всегда иметь актуальную информацию
     * @return \yii\db\ActiveQuery
     */
    public function getTaskPauses()
    {
        /*
         * Сохраняем паузы в кэш и сбросим кэш когда будем создавать новые паузы или делать активные не активными
         * Ok
         */
        $key = TaskPause::CACHE_ACTIVE_BY_TASK_ID_KEY . $this->id;
        $taskPauses = \Yii::$app->cache->get($key);
        if (!$taskPauses) {
            $taskPauses = TaskPause::findAll(['task' => $this->id, 'is_active' => true]);
            $dependency = new TagDependency(['tags' => [
                deficit::DEP_COMMON_TAG,
                deficit::DEP_PAUSE_TAG_PREFIX . $this->id
            ]]);

            \Yii::$app->cache->set($key, $taskPauses, deficit::PAUSE_DURATION, $dependency);
        }
        return $taskPauses;
    }

    /**
     * Геттер для статуса задачи
     * @return TaskStatus
     */
    public function getTaskStatus()
    {
        /*
         * Сохраняем статус в кэш по id. Статусы в нормальной ситуации не меняются никогда, и
         * раз сохраняем/ищем по id самого статуса, то сохраняем надолго.
         * Ok
         */
        $key = TaskStatus::CACHE_BY_ID_KEY . $this->status;
        $taskStatus = \Yii::$app->cache->get($key);
        if (!$taskStatus) {
            $taskStatus = TaskStatus::findOne(['id' => $this->status]);
            $dependency = new TagDependency(['tags' => [deficit::DEP_COMMON_TAG]]);
            \Yii::$app->cache->set($key, $taskStatus, deficit::COMM_DURATION, $dependency);
        }
        return $taskStatus;
    }

    /**
     * Геттер для типа задачи
     *
     * @return TaskType
     * @throws \Exception
     */
    public function getTaskType()
    {
        /*
         * Типы задач в идеале вообще не меняются, кэшируем надолго
         * Ok
         */
        $key = TaskType::CACHE_BY_ID_KEY . $this->type;
        $taskType = \Yii::$app->cache->get($key);
        if (!$taskType) {
            $taskType = TaskType::findOne(['id' => $this->type]);
            $dependency = new TagDependency(['tags' => deficit::DEP_COMMON_TAG]);
            \Yii::$app->cache->set($key, $taskType, deficit::COMM_DURATION, $dependency);
        }
        return $taskType;
    }

    /**
     * Отношени для дочерних задач
     * @return \yii\db\ActiveQuery
     */
    public function getChildrenTasks()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    /**
     * Получить всех потомков задачи рекурсивно
     * @return Task[]
     */
    public function getAllChildrenTasks()
    {
        $childrenTasks = [];
        foreach ($this->childrenTasks as $childrenTask) {
            $childrenTasks[] = $childrenTask;
            if ($childrenTask->childrenTasks) {
                $childrenTasks = array_merge($childrenTasks, $childrenTask->getAllChildrenTasks());
            }
        }

        return $childrenTasks;
    }

    /**
     * Отношение для родительской задачи
     * @return \yii\db\ActiveQuery
     */
    public function getParenTask()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * Получить предковую задачу
     * @return Task
     */
    public function getAncestorTask()
    {
        $ancestorTask = $this->parenTask;
        if ($ancestorTask->parenTask) {
            $ancestorTask = $ancestorTask->getAncestorTask();
        }
        return $ancestorTask;
    }

    /**
     * Отношение для данных таски
     * @return \yii\db\ActiveQuery
     */
    public function getTaskData()
    {
        return $this->hasOne(TaskData::className(), ["task_id" => "id"]);
    }

    public function validateStatusNotError($attribute, $params)
    {
        if ($this->getTaskStatus()->is_error === true) {
            $errorMessage = "Задача с id {$this->id} находится в статусе 'Ошибка', продолжение работы невозможно";
            $this->addError($attribute, $errorMessage);
            $this->exception = new StatusMismatchException($errorMessage);
        }
    }

    public function validateStatusNotSuccess($attribute, $params)
    {
        if ($this->getTaskStatus()->is_success === true) {
            $errorMessage = "Ожидается, что задача № {$this->id} не будет в статусе 'успешно завершена'";
            $this->addError($attribute, $errorMessage);
            $this->exception = new TaskAlreadySuccessfulException($errorMessage);
        }
    }

    public function validateStatusNotFirst($attribute, $params)
    {
        if ($this->getTaskStatus()->is_new === true) {
            $errorMessage = "Задача с id {$this->id} находится в статусе 'Новая задача', продолжение работы невозможно (возможно не было вызова continueTask() после создания задачи).";
            $this->addError($attribute, $errorMessage);
            $this->exception = new StatusMismatchException($errorMessage);
        }
    }

    public function validateStatusByCmd($attribute, $params)
    {
        if ($this->getTaskStatus()->command != $this->commandName) {
            $errorMessage = "Ожидался статус соответствующий команде {$this->commandName}, но текущий статус: {$this->taskStatus->id}";
            $this->addError($attribute, $errorMessage);
            $this->exception = new StatusMismatchException($errorMessage);
        }
    }

    public function validateTaskActive($attribute, $params)
    {
        $activePauses = $this->getTaskPauses();
        if (!empty($activePauses)) {
            $errorMessage = "Для задачи есть активные паузы";
            $this->addError($attribute, $errorMessage);
            $this->exception = new TaskPausedException($errorMessage);
        }
    }

    public function validateRelated($attribute, $params)
    {
        if ($this instanceof Task && $this->related === null) {
            $errorMessage = "В задаче не указан id связанной с ней сущности";
            $this->addError($attribute, $errorMessage);
            $this->exception = new NoRelatedException($errorMessage);
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->is_unqueued = ($this->getTaskStatus()->is_unqueued === true) ? true : false;
            $this->is_success = ($this->getTaskStatus()->is_success === true) ? true : false;
            $this->is_error = ($this->getTaskStatus()->is_error === true) ? true : false;
            return true;
        }
        return false;
    }
}
