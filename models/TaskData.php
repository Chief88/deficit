<?php

namespace chief88\deficit\models;

use Yii;

/**
 * Модель для дополнительных данных задачи
 *
 * @property integer $id
 * @property integer $task_id
 * @property string $data
 *
 * @property DeficitTask $task
 */
class TaskData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_data';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'errorToException' => [
                'class' => 'chief88\deficit\behaviors\ErrorToException',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'data'], 'required'],
            [['task_id'], 'integer'],
            [['data'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(DeficitTask::className(), ['id' => 'task_id']);
    }
}