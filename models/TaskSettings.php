<?php

namespace chief88\deficit\models;

use yii\db\ActiveRecord;

class TaskSettings extends ActiveRecord
{
    const CACHE_BY_TYPE_KEY = 'deficit_cache_settings_prefix_';

    public static function tableName()
    {
        return 'task_settings';
    }

    public function behaviors()
    {
        return [
            'createdAt' => [
                'class' => 'chief88\deficit\behaviors\CreatedAt',
            ],
        ];
    }
}
