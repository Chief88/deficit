<?php

namespace chief88\deficit\models;

use chief88\deficit\exceptions\StatusNotFoundException;
use chief88\deficit\components\DeficitComponent as deficit;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;

/**
 * Модель статуса для задачи для activemq
 *
 * @property integer $id
 * @property integer $task_type
 * @property string $slug
 * @property string $command
 * @property string $title
 * @property string $description
 * @property integer $next
 * @property integer $previous
 * @property string $created
 * @property string $is_new
 * @property string $is_error
 * @property string $is_unqueued
 * @property string $is_success
 * @property integer $waiting
 * @property integer $repeat_counter
 * @property string $injection
 *
 */
class TaskStatus extends ActiveRecord
{
    const CACHE_BY_ID_KEY = 'deficit_cache_status_by_id_';
    const CACHE_FIRST_BY_TYPE_ID_KEY = 'deficit_cache_first_status_by_type_id_';
    const CACHE_ERROR_BY_TYPE_ID_KEY = 'deficit_cache_error_status_by_type_id_';
    const CACHE_SUCCESS_BY_TYPE_ID_KEY = 'deficit_cache_success_status_by_type_id_';

    public static function tableName()
    {
        return 'task_status';
    }

    public function behaviors()
    {
        return [
            'createdAt' => [
                'class' => 'chief88\deficit\behaviors\CreatedAt',
            ],
        ];
    }

    /**
     * Геттер для следующего статуса
     * Возвращает для объекта статуса следующий статус.
     *
     * Через внешний ключ к строке в той же таблице находит следующий статус и если такой существует возвращает такой.
     * Для последнего статуса и для статуса "ошибка" не вернёт ничего и выбросит исключение.
     *
     * @throws StatusNotFoundException
     * @throws \Exception
     * @return TaskStatus
     */
    public function getNextStatus()
    {
        /*
         * Ok
         */
        $key = self::CACHE_BY_ID_KEY . $this->next;
        $nextStatus = \Yii::$app->cache->get($key);

        if (!$nextStatus) {
            $nextStatus = TaskStatus::findOne(['id' => $this->next]);
            $dependency = new TagDependency(['tags' => deficit::DEP_COMMON_TAG]);
            \Yii::$app->cache->set($key, $nextStatus, deficit::COMM_DURATION, $dependency);
        }
        if (!isset($nextStatus)) throw new StatusNotFoundException("Не найден следующий статус для статуса $this->description с id № $this->id");
        return $nextStatus;
    }

    /**
     * Геттер для предыдущего статуса
     *
     * @throws StatusNotFoundException
     * @throws \Exception
     * @return TaskStatus
     */
    public function getPreviousStatus()
    {
        /*
         * Ok
         */
        $key = self::CACHE_BY_ID_KEY . $this->previous;
        $previousStatus = \Yii::$app->cache->get($key);

        if (!$previousStatus) {
            $previousStatus = TaskStatus::findOne(['id' => $this->previous]);
            $dependency = new TagDependency(['tags' => deficit::DEP_COMMON_TAG]);
            \Yii::$app->cache->set($key, $previousStatus, deficit::COMM_DURATION, $dependency);
        }
        if (!isset($previousStatus)) throw new StatusNotFoundException("Не найден предыдущий статус для статуса $this->description с id № $this->id");
        return $previousStatus;
    }
}