<?php

namespace chief88\deficit\models;

use yii\db\ActiveRecord;

class TaskLog extends ActiveRecord
{
    public static function tableName()
    {
        return 'task_log';
    }

    public function behaviors()
    {
        return [
            'createdAt' => [
                'class' => 'chief88\deficit\behaviors\CreatedAt',
            ],
        ];
    }
}