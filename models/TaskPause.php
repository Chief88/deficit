<?php

namespace chief88\deficit\models;

use yii\caching\TagDependency;
use yii\db\ActiveRecord;
use chief88\deficit\components\DeficitComponent as deficit;

class TaskPause extends ActiveRecord
{
    const CACHE_ACTIVE_BY_TASK_ID_KEY = 'cache_active_by_task_';

    public static function tableName()
    {
        return 'task_pause';
    }

    public function behaviors()
    {
        return [
            'createdAt' => [
                'class' => 'chief88\deficit\behaviors\CreatedAt',
            ],
            'updatedAt' => [
                'class' => 'chief88\deficit\behaviors\UpdatedAt',
            ],
        ];
    }

    public function getRelatedTask()
    {
        return $this->hasOne(Task::className(), ["id" => "task"]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        TagDependency::invalidate(\Yii::$app->cache, self::CACHE_ACTIVE_BY_TASK_ID_KEY . $this->task);
    }
}