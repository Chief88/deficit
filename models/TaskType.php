<?php

namespace chief88\deficit\models;

use yii\db\ActiveRecord;

/**
 * Модель типа задачи для activemq
 *
 * @property integer $id
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property string $queue
 * @property string $created
 * @property string $broker_name
 * @property integer $injection
 * @property bool $persist_messages
 * @property string $log_group
 *
 */
class TaskType extends ActiveRecord
{
    const CACHE_BY_ID_KEY = 'deficit_cache_type_by_id_';
    const CACHE_BY_SLUG_KEY = 'deficit_cache_type_by_slug_';

    public static function tableName()
    {
        return 'task_type';
    }

    public function behaviors()
    {
        return [
            'createdAt' => [
                'class' => 'chief88\deficit\behaviors\CreatedAt',
            ],
        ];
    }

    public function getTaskStatuses()
    {
        return $this->hasMany(TaskStatus::className(), ["task_type" => "id"]);
    }

    public function getTaskSettings()
    {
        return $this->hasMany(TaskSettings::className(), ["task_type" => "id"]);
    }

    public function getTaskPauses()
    {
        return $this->hasMany(TaskPauseType::className(), ["task_type" => "id"]);
    }
}
