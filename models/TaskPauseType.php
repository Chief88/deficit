<?php

namespace chief88\deficit\models;

use yii\db\ActiveRecord;

class TaskPauseType extends ActiveRecord
{
    const CACHE_KEY = 'deficit_cache_pause_type_prefix_';

    public static function tableName()
    {
        return 'task_pause_type';
    }

    public function behaviors()
    {
        return [
            'createdAt' => [
                'class' => 'chief88\deficit\behaviors\CreatedAt',
            ],
        ];
    }
}
