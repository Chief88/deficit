<?php

namespace chief88\deficit\models;
use chief88\deficit\exceptions\InvalidTaskTypeException;


/**
 * @property TaskData $relatedData Дополнительные данные по задаче
 */
class ExtendedTask extends Task
{
    /**
     * @var object $data Объект дополнительных данных для задачи. Подробности смотри в описании к геттеру и сеттеру.
     * @see chief88\deficit\models\ExtendedTask::getData
     * @see chief88\deficit\models\ExtendedTask::setData
     */
    protected $data;

    /**
     * Отношение для дополнительных данных по задаче
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedData()
    {
        return $this->hasOne(TaskData::className(), ['task_id' => 'id']);
    }

    /**
     * Заполним отношение relatedData, чтобы потом сохранить вместе с задачей
     * @param array $data
     * @return bool
     */
    public function setRelatedData(array $data)
    {
        $taskData = new TaskData();
        $taskData->data = json_encode($data);
        $this->populateRelation('relatedData', $taskData);
        return true;
    }

    /**
     * Вместе с задачей сохраним и дополнительные данные к ней
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $isRunTransaction = true;
        if (isset(\Yii::$app->db->transaction) && \Yii::$app->db->transaction->getLevel() != 0) {
            $isRunTransaction = false;
        }
        if ($isRunTransaction) {
            $transaction = \Yii::$app->db->beginTransaction();
        }
        try {
            parent::save($runValidation, $attributeNames);
            $this->relatedData->task_id = $this->id;
            $this->relatedData->save();
            if ($isRunTransaction) {
                $transaction->commit();
            }
        } catch (\Exception $e){
            if ($isRunTransaction) {
                $transaction->rollback();
            }
            throw $e;
        }
        return true;
    }

    /**
     * После того, как нашли задачу, заполним в $this->data из базы и дополнительные данные к ней
     * @inheritdoc
     */
    public function afterFind()
    {
        if (empty($this->relatedData)) {
            throw new InvalidTaskTypeException();
        }
        $this->data = json_decode($this->relatedData->data);
        parent::afterFind();
    }

    /**
     * Получим дополнительные данные в виде простого объекта, поля которого соответствуют элементам массива $data,
     * который был передан при создании задачи в DeficitComponent::createTask()
     * @return object
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Заполнить дополнительные данные к задаче и релейшен для них
     * @param array $data
     * @return bool
     */
    public function setData(array $data)
    {
        $this->data = (object) $data;
        $this->relatedData = $data;
        return true;
    }
}