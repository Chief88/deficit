<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если не удаётся найти тип задачи.
 *
 * Class NoTaskTypeException
 */
class NoTaskTypeException extends \Exception
{

}