<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если не удаётся получить задачу.
 *
 * Class NoTaskException
 */
class NoTaskException extends \Exception
{

}