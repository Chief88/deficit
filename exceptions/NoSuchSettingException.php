<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если не удаётся получить настройку с таким ключом.
 *
 * Class NoSuchSettingException
 */
class NoSuchSettingException extends \Exception
{

}