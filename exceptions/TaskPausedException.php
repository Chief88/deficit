<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если задача стоит на паузе и совершается попытка выполнить
 * задачу.
 *
 * Class TaskPausedException
 */
class TaskPausedException extends \Exception
{

}