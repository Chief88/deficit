<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если задача уже успешно завершена и совершается попытка продолжить
 * с ней работу
 *
 * Class TaskAlreadySuccessfulException
 */
class TaskAlreadySuccessfulException extends \Exception
{

}