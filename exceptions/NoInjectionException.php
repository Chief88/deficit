<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если нет объекта реализующего логику задачи.
 *
 * Class NoInjectionException
 */
class NoInjectionException extends \Exception
{

}