<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если в метод отправки лог соощения попадает ассоциативный
 * массив в котором в значениях есть что-то кроме строк.
 *
 * Class NoTaskException
 */
class NotStringException extends \Exception
{

}