<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если для обычной задачи (не для расширенной, которая может
 * иметь свои данные) не указан id связанной сущности.
 *
 * Class NoRelatedException
 */
class NoRelatedException extends \Exception
{

}