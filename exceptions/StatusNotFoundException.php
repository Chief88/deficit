<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если попытка получить статус для задачи (следующий, или
 * предыдущий не увенчалась успехом)
 *
 * Class StatusNotFoundException
 */
class StatusNotFoundException extends \Exception
{

}