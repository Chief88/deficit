<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если не удаётся получить тип паузы для задачи.
 *
 * Class NoTaskPauseTypeException
 */
class NoTaskPauseTypeException extends \Exception
{

}