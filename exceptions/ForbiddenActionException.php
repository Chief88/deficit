<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если совершаемое действие нарушает логику работы модуля.
 *
 * Class NoTaskException
 */
class ForbiddenActionException extends \Exception
{

}