<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если ожидался тип задачи ExtendedTask, а пришел Task,
 * или наоборот.
 *
 * Class InvalidTaskTypeException
 */
class InvalidTaskTypeException extends \Exception
{

}