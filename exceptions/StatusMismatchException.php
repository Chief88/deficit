<?php

namespace chief88\deficit\exceptions;

/**
 * Данный тип исключения выбрасывается в том случае, если задача имеет некорректный статус для начала работы.
 *
 * Class StatusMismatchException
 */
class StatusMismatchException extends \Exception
{

}