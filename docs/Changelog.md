История изменений
============================

* 0.1.5
  * Добавлены дополнительные исключения: TaskPausedException, TaskAlreadySuccessfulException, StatusMismatchException,
  NoTaskException, NoRelatedException, NoInjectionException. Все исключения кроме TaskAlreadySuccessfulException и 
  TaskPausedException переводят задачу в статус "ошибка", в случае если проблема возникает на этапе работы метода 
  beginWorking(), после чего исключения пробрасываются дальше.
   