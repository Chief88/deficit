Пример создания задачи для асинхронной отправки почты
============================

Исходим из того что у нас уже имеется развёрнутое прилжение Yii2.

Выполняем действия указанные в [руководстве по установке](docs/Install.md).

Как создать тип задачи
============================

Создаём миграцию для накатывания типа задачи. В метод safeUp добавим следующий код:

```php        
        
            public function safeUp()
            {
                $data = [
                    /*
                     * Определяем информацию о типе задачи
                     */
                    'task_type' => [
                        'slug' => 'AsyncEmail', //КО: уникальный слаг задачи
                        'title' => 'Отправка письма', //КО: название задачи
                        'description' => 'Асинхронная отправка письма через сервер очередей', //КО: название подробнее
                        /*
                         * Уникальное название очереди на сервере сообщений, в которую будут отправляться сообщения и откуда
                         * потом они будут извлекаться консюмерами для выполнения работы.
                         */
                        'queue' => 'send_email',
                        /*
                         * Название компонента Yii2 в том виде в каком он прописан в конфигурационном файле. На текущий момент
                         * это небольшое легаси, на текущий момент в большинстве случаев нужен injection указаный только
                         * в статусах (это ниже).
                         * Данный компонент реализует логику выполнения задачи
                         */
                        'injection' => 'AsyncEmail',
                        /*
                         * Название дополнительной очереди, куда будут отправляться лог-сообщения которые генерирует задача
                         * при работе. Сообщения отправляются методами класса chief88\deficit\components\Log. Если подходит
                         * стандартное логирование Yii2 в файлы, можно просто не пользоваться логированием данного модуля.
                         */
                        'log_group' => 'send_email_logs',
                        /*
                         * Флаг который определяет должны ли быть сообщения отправляемые в очередь персистентными.
                         * (Персистентные сообщения переживают перезагрузку сервера очередей).
                         * Персистентные сообщения надёжнее, но работают медленее.
                         */
                        'persist_messages' => false,
                        /*
                         * Здесь ещё может быть параметр который сообщает о том на каком хосте находится сервер очередей.
                         * По-умолчанию считается, что это хост который в массиве 
                         * $config['components']['deficitQueue']['brokers'] доступен по ключу localhost.
                         */
                    ],
                    /*
                     * Здесь перечисляются статусы, через которые последовательно проходит задача при выполнении работы.
                     * Например:
                     * 1) Отправили куда-то запрос по апи
                     * 2) Проверили по апи готов ли результат (при необходимости подождали и повторили)
                     * 3) Спустя какое-то время, когда получили ответ что результат готов, выполнили какие-то действия
                     * Разбиение задачи на этапы имеет смысл в основном только в том случае если этапы выполнения задачи
                     * должны быть разнесены по времени. Иначе всё можно сделать в одно действие.
                     */
                    'statuses' => [
                        /*
                         * С отправкой почты можно справиться за один подход, поэтому один статус
                         */
                        [
                            'slug' => 'send_email', //КО: уникальный слаг статуса
                            'title' => 'Отправка письма', //КО: Уникальное название статуса
                            /*
                             * Консольная команда, при выполнении которой происходит работа (отправка почты).
                             * Как всё работает:
                             * 1) Создаётся задача в базе данных, сообщение уходит в очередь
                             * 2) Где-то работает консюмер (консольное приложение), который умеет извлекать сообщения из
                             * очереди и запускать консольные команды yii2 (которая и прописывается в поле 'command').
                             * 3) Этот консюмер извлекает сообщение из очереди, в сообщении есть информация о команде которую
                             * нужно запустить (в нашем случае это будет 'deficit/email/send') и о id задачи которую
                             * необходимо выполнить. И консюмер выполяет shell_exec() запуская команду
                             * yii deficit/email/send N где N - это id задачи, primary key из таблицы task
                             * 4) Консольная команда выполняет свою работу.
                             */
                            'command' => 'deficit/email/send',
                            'description' => 'Асинхронная отправка почты', //КО: описание задачи
                            /*
                         * Название компонента Yii2 в том виде в каком он прописан в конфигурационном файле.
                         * Данный компонент реализует логику выполнения задачи
                         */
                            'injection' => 'AsyncEmail',
                        ],
                    ],
                    /*
                     * Различные параметры которые нужны для выполнения задачи. Логины, пароли, явки, значения параметров
                     * конфигурации.
                     */
                    'settings' => [
                        'sender_address' => [
                            'value' => 'no-reply@host.com',
                            'description' => 'Адрес для указания в качестве отправителя'
                        ],
                    ],
                    /*
                     * Список пауз, которые могут понадобиться чтобы приостановить выполнение какой-нибудь задачи по
                     * отправке почты. Не представляю, что это может понадобиться при отправке почты. Поэтому пусто.
                     */
                    'pause_types' => [
                    ]
                ];
        
                TypeEditor::createDbEnvironment($data);
            }
```

В метод safeDown добавим метод откатывающий транзакцию:

```php
    public function safeDown()
    {
        TypeEditor::rollbackDbEnvironment('send_email');
    }
```

При изменении структуры задачи в базе данных (статусов, настроек, и т.п.) необходимо сбрасывать кэш, потому как
эти параметры для повышения быстродействия кэшируются. Для сброса кэша относящегося только к deficit есть
специальная команда:
```
./yii deficit/cache/invalidate-all
```

Создание компонента реализующего логику работы с задачей
============================

На данном этапе нам нужно создать компонент который будет реализовывать интерфейс DeficitInjection и в конфигурационном файле будет прописан с тем
    ключом который мы указали при создании типа задачи в миграции в параметре с ключом injection. То есть фрагмент конфигурационного файла будет иметь следующий вид:

    ```
    'components' => [
            //.......................
            'Weather' => [
                'class' => 'app\components\Weather',
            ],
        ],
    ```

Приблизительный вид компонента:
```
    <?php
    
    namespace app\components;
    
    
    use chief88\deficit\injections\DeficitInjection;
    use yii\base\Component;
    
    class Weather extends Component implements DeficitInjection{
    
        const ACTION_REPEAT = "action_repeat";
        const ACTION_PAUSE = "action_pause";
        const ACTION_ERROR = "action_error";
        const ACTION_SUCCESS = "action_success";
        const ACTION_CONTINUE = "action_continue";
    
        private $weather;
    
        public function handleErrors(){
    
        }
    
        public function buildLogData($data){
            if(!isset($weather)){
                $weather_id = null;
            }else{
                $weather_id = $this->weather->id;
            }
            $t = microtime(true);
            $micro = sprintf("%06d",($t - floor($t)) * 1000000);
            $d = new \DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
            $now = $d->format("Y-m-d H:i:s.u");
    
            $data = [
                'related_id' => $weather_id,
                'description' => $data['description'],
                'message' => $data['message'],
                'datetime' => $now,
                'microtime' => $t
            ];
            return $data;
        }
    
        public function setInfoObject($infoObject){
            $this->weather = $infoObject;
    
        }
    }    
```    

Как создать задачу в базе и отправить сообщение в очередь
============================

Вызываем метод компонента:

```
$task = Yii::$app->deficitComponent->createTask("get_weather", $weather->id, $description);
```
Где в createTask первый параметр это slug из таблицы типов задач, второй параметр это id сущности которая
содержит информацию о самой задаче, это может быть платёж, запрос по API и всё что угодно. Третий параметр просто
описание.

После вызова этого метода создалась задача со статусом "новая", это задача никуда не отправлена и не передана
в работу. Чтобы задача поступила в работу нужно вызвать метод:
```
Yii::$app->deficitComponent->continueTask($task);
```
И задача будет передана в очередь.

Конкретный пример создания задачи
============================

Чтобы побыстрее получить что-то работающее задачу будем создавать через командную строку.

Как выполнить задачу
============================

В нашей структуре первой подзадачей является генерация запроса который уйдёт по API. Это работа должна быть выполнена
командой weather-generate-request, как это видно из кода выше:

```
            'statuses' => [
                [
                    'slug' => 'generate_request',
                    'title' => 'Генерация запроса',
                    'command' => 'weather-generate-request',
                    'description' => 'Генерация запроса который будет отправлен по API',
                ],
                .......
```

При получении сообщения потребитель выполняет команду weather-generate-request и параметром передаёт ей id задачи.

```
        //по известному id получим наше задание
        $task = Task::findOne(["id" => $taskId]);
        //найдём связанную сущность
        $weather = Weather::findOne(["id" => $task->related]);
        /*
         * теперь выполняем офигенно нетривиальную работу
         * но нам сейчас главное продемонстрировать принцип
         * в другой раз это может быть тяжёлая сборка xml для запроса
         * которая использует ФИАС и ещё бог знает что это может быть
         */
        $url = "http://api.openweathermap.org/data/2.5/weather?q=$weather->name";
        $weather->req_url = $url;
        $weather->save();

        //выполняем переход к следующему этапу
        \Yii::$app->deficitComponent->continueTask($task);
```

И так повторяем, пока не будет выполнена последняя команда