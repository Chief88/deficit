О версиях
============================
0.1.3 - Текущая актуальная версия, всё что младшей версии устарело и не поддерживается

0.2.6 - После долгого перерыва продолжено описание изменений. В 0.2.6 был переработан алгоритм переотправки
задач в очередь, который ранее работал относительно долго. Проведена небольшая денормализация, теперь не нужно
джойнить статусы чтобы узнать, что задача "не в очереди". Запрос для получения задач без пауз в статусе
"не в очереди" стал работать быстрее примерно в 1000-1500 раз.

Запрос до 0.2.6

```
Hash Anti Join  (cost=457.27..124513.89 rows=610128 width=79) (actual time=956.711..956.711 rows=0 loops=1)
  Hash Cond: (task.id = task_pause.task)
  ->  Hash Join  (cost=12.25..116270.73 rows=611158 width=79) (actual time=428.607..954.541 rows=542 loops=1)
        Hash Cond: (task.status = task_status.id)
        ->  Seq Scan on task  (cost=0.00..100619.39 rows=2540671 width=79) (actual time=0.013..637.291 rows=2540245 loops=1)
              Filter: (id < 3793848)
        ->  Hash  (cost=11.37..11.37 rows=70 width=4) (actual time=0.388..0.388 rows=70 loops=1)
              Buckets: 1024  Batches: 1  Memory Usage: 11kB
              ->  Index Scan using deficit_task_status_is_unqueued on task_status  (cost=0.15..11.37 rows=70 width=4) (actual time=0.022..0.365 rows=70 loops=1)
                    Index Cond: (is_unqueued = true)
                    Filter: (is_unqueued IS TRUE)
  ->  Hash  (cost=391.52..391.52 rows=4280 width=4) (actual time=1.914..1.914 rows=1220 loops=1)
        Buckets: 8192  Batches: 1  Memory Usage: 107kB
        ->  Index Scan using pause_active_idx on task_pause  (cost=0.29..391.52 rows=4280 width=4) (actual time=0.036..1.630 rows=1220 loops=1)
              Index Cond: (is_active = true)
              Filter: (is_active IS TRUE)
Planning time: 0.787 ms
Execution time: 956.798 ms
```

Этот же функционал в 0.2.6

```
Nested Loop Anti Join  (cost=0.72..47.51 rows=1 width=79) (actual time=0.618..0.618 rows=0 loops=1)
  ->  Index Scan using deficit_task_unqueued on task  (cost=0.43..31.19 rows=1 width=79) (actual time=0.015..0.183 rows=165 loops=1)
        Index Cond: (is_unqueued = true)
        Filter: ((is_unqueued IS TRUE) AND (is_success IS FALSE) AND (is_error IS FALSE) AND (id < 3793848))
  ->  Index Scan using task_pause_task_index on task_pause  (cost=0.29..8.31 rows=1 width=4) (actual time=0.002..0.002 rows=1 loops=165)
        Index Cond: (task = task.id)
        Filter: (is_active IS TRUE)
Planning time: 0.294 ms
Execution time: 0.648 ms

```

0.2.7 - Исправлена ошибка при отправке сообщения в логи без привязки к конкретной задаче. Теперь корректно используется
информация о типе задачи и лог-сообщения уходят без ошибок.

0.2.8 - В редактор типов добавлена возможность создавать настройки для типа задачи после создания задачи.