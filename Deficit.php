<?php

namespace chief88\deficit;

use yii\base\BootstrapInterface;
use \yii\console\Application;
use \yii\base\Module;

class Deficit extends Module implements BootstrapInterface
{
    public $controllerNamespace = 'chief88\deficit\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        if ($app instanceof Application) {
            $this->controllerNamespace = 'chief88\deficit\commands';
        }
    }
}
