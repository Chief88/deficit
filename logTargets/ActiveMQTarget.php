<?php

namespace chief88\deficit\logTargets;

use chief88\deficit\exceptions\NoTaskException;
use chief88\deficit\models\TaskLog;
use Yii;
use chief88\deficit\components\Queue;
use yii\log\Logger;
use yii\log\Target;

/**
 * ActiveMQTarget отправляет логи в очередь ActiveMQ.
 *
 */
class ActiveMQTarget extends Target
{

    /**
     * Просто повторяющийся код
     *
     * @param $message
     * @param $level
     */
    private function saveLogToDatabase($message, $level){
        $log = new TaskLog();
        try{
            $log->task = \Yii::$app->deficitComponent->getTask()->id;
        }catch(NoTaskException $e){

        }
        $log->message = json_encode($message);
        $log->level = $level;
        $log->save();
    }

    /**
     * Отправляет лог-сообщения в очередь ActiveMQ. Если отправка не удаётся, сохраняет логи в базу.
     * Если имела место ошибка, то ещё отправляет письмо на почту.
     */
    public function export()
    {
        /** @var Queue $deficitQueue */
        $deficitQueue = \Yii::$app->deficitQueue;

        $deficit = \Yii::$app->deficitComponent;

        foreach ($this->messages as $message) {
            list($message, $level, $category, $timestamp) = $message;
            /*
             * Если это сообщение о ошибке, отправим на почту
             */
            if($level == Logger::LEVEL_ERROR){
                $email = isset($deficit->getInjection()->support_email) ? $deficit->getInjection()->support_email : $deficit->default_support_email;

                $message_string = "";
                foreach($message as $name => $data){
                    $message_string .= "{$name}: {$data}\n";
                }
                Yii::$app->mailer->compose()
                    ->setFrom('deficit@error.log')
                    ->setTextBody($message_string)
                    ->setTo($email)
                    ->setSubject('deficit error log')
                    ->send();
            }

            /*
             * Если для компонента выключено отладочное логирование и это отладочное сообщение, то пропустим его.
             */
            if($level == Logger::LEVEL_TRACE && !$deficit->getInjection()->debug)
                continue;

            if(!$deficitQueue->send(
                $message['log_queue_name'],
                json_encode(array_merge(
                    $message, [
                    'timestamp' => $timestamp,
                    'datetime' => date('d-m-Y H:i:s', $timestamp)
                ])), [], true)
            ){
                /*
                 * Если не удалось отправить сообщение в очередь
                 */
                $this->saveLogToDatabase($message, $level);
            }
        }
    }
}
