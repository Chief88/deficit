<?php

namespace chief88\deficit\commands;

use Elastica\Client;
use Elastica\Document;
use yii\console\Controller;


class ConsumerElasticController extends Controller
{
    public function actionIndex()
    {
        $log_queue = \Yii::$app->deficitComponent->log_queue;

        try {
            $subscribe_result = \Yii::$app->deficitComponent->stomp->subscribe($log_queue);
            $client = new Client();
            $index = $client->getIndex('payouts');
//            $index->create([]);

            while(true) {
                $frame = \Yii::$app->deficitComponent->stomp->readFrame();
                if($frame) {
                    if($frame->command == "MESSAGE"){
                        $body = json_decode($frame->body, true);
                        if(!is_array($body)) throw new \Exception("Логируемая информация должна быть массивом. Стоит проверить то что отправляется на логирование.");
                        $type = $index->getType('payouts');
                        $type->addDocument(new Document("", $body));
                        $index->refresh();
                        \Yii::$app->deficitComponent->stomp->ack($frame);
                    }
                }
            }
        } catch(\Exception $e) {
            echo "\nException: ".$e->getMessage()."\n".$e->getTraceAsString();
        }
    }
}
