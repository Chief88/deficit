<?php

namespace chief88\deficit\commands;

use yii\console\Controller;


class ResenderController extends Controller
{
    public function actionIndex()
    {
        try {
            $start = microtime(true);
            echo "\n----------------------------------------------------------------------------------------";
            $datetime = new \DateTime("now");
            echo "\n||".$datetime->format('d-m-Y H:i:s');
            $count = \Yii::$app->deficitComponent->resendTasksToQueue();

            $time = microtime(true) - $start;
            printf("\n||Обработано $count пауз.");
            printf("\n||Скрипт выполнялся %.4F сек.", $time);
            echo "\n||";
            echo "\n----------------------------------------------------------------------------------------\n";

        } catch(\Exception $e) {
            $break = true;
        }
    }
}
