<?php

namespace chief88\deficit\commands;

use chief88\deficit\components\Queue;
use yii\console\Controller;


class ConsumerController extends Controller
{
    public function actionIndex($queue, $broker_name = null)
    {
        if(!isset($broker_name)){
            $broker_name = 'localhost';
        }

        if(!isset($queue)) throw new \Exception("Не задано имя очереди ActiveMQ");
        try {
            /** @var Queue $deficitQueue */
            $deficitQueue = \Yii::$app->deficitQueue;
            $deficitQueue->subscribe($queue, $broker_name);

            $appDir = \Yii::getAlias('@app'); 
            while(true) {
                if ( file_exists("$appDir/deficit_maintenance.lock") || file_exists("$appDir/$queue.lock") ){
                    sleep(10);
                    continue;
                }
                $message = $deficitQueue->getMessage();
                if($message){
                    $cmd = PHP_BINDIR."/php $appDir/yii $message->command $message->task_id";
                    shell_exec($cmd);
                }
            }
        } catch(\Exception $e) {
            echo "\nException: ".$e->getMessage()."\n";
        }
    }
}
