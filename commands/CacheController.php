<?php

namespace chief88\deficit\commands;

use yii\caching\TagDependency;
use yii\console\Controller;
use chief88\deficit\components\DeficitComponent as deficit;
use yii\helpers\Console;


class CacheController extends Controller
{
    public function actionIndex()
    {
        echo "\nNo any action\n";
    }

    public function actionInvalidateAll()
    {
        if ($this->confirm(
            "\nБудет сброшен весь кэш (это безопасное действие) для модуля deficit (статусы, настройки, паузы) вы ".
            "хотите продолжить?",
            false
        )) {
            TagDependency::invalidate(\Yii::$app->cache, deficit::DEP_COMMON_TAG);
            $this->stdout("\nКэш был инвалидирован.\n", Console::FG_GREEN);
            return;
        }else{
            $this->stdout("\nВы отказались от инвалидации кэша.\n", Console::FG_GREY);
            return;
        }
    }
}
