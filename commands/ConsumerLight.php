<?php

class ConsumerLight
{
    const DUMMY = null;

    private $vendor_dir = self::DUMMY;

    private $app_dir = self::DUMMY;

    private $deficit_config = self::DUMMY;

    private $log_dir = self::DUMMY;

    /** @var \CentralDesktop\Stomp\Connection */
    private $connection = null;

    private $queue = null;

    private $host = null;

    /**
     * Счётчик сообщений, чтобы логировать память не постоянно, а, допустим, после каждого 1000го сообщения
     * @var int
     */
    private $messages_count = 0;

    /**
     * Строка в которой хранится уникальный идентификатор консюмера, например DEANGELO_JACKSON_5837
     * Требуется для логирования информации о потреблении памяти
     * @var string|null
     */
    private $process_uid = null;
    /**
     * Афроамериканские имена
     * @var array
     */
    private $names = ['ANDRE','ANTOINE','ANTWAN','BRAYLON','DAQUAN','DEANDRE','DEANGELO','DEDRICK','DEION','DEON',
        'DEONTE','DESHAUN','DESHAWN','IESHA','IMANI','IVORY','JALEN','JAMAAR','JAMAR','JAQUAN','JAYLEN','JAYLIN',
        'JAYLON','KALISHA','KEISHA','KENYA','KESHAUN','KESHAWN','KESHIA','KEYSHA','KIANA','KISHA','LADONNA','LAGINA',
        'LAKEISHA','LAKESHIA','LAKISHA','LAMAR','LASHAWN','LASHAY','LASHONDA','LATANYA','LATASHA','LATISHA','LATONYA',
        'LATOYA','LAWANDA','MARQUES','MARQUIS','MARQUISE','MARQUITA','NATISHA','NIKEISHA','QIANA','QUANNA','QUIANA',
        'QUIANNA','RASHAUN','RASHAWN','SHANICE','SHANIKA','SHANIQUA','SHELENA','TAJUANA','TALISHA','TANEKA','TANESHA',
        'TANIKA','TANIQUA','TANISHA','TISHA','TORY','TYREEK','TYRESE','TYRIK','TYRIQ'];
    /**
     * Афроамериканские фамилии
     * @var array
     */
    private $last_names = ['WILLIAMS','JOHNSON','SMITH','JONES','BROWN','JACKSON','DAVIS','THOMAS','HARRIS','ROBINSON',
        'TAYLOR','WILSON','MOORE','WHITE','LEWIS','WALKER','GREEN','WASHINGTON','THOMPSON','ANDERSON','SCOTT','CARTER',
        'WRIGHT','MILLER','HILL','ALLEN','MITCHELL','YOUNG','LEE','MARTIN','CLARK','TURNER','HALL','KING','EDWARDS',
        'COLEMAN','JAMES','EVANS','BELL','RICHARDSON','ADAMS','BROOKS','PARKER','JENKINS','STEWART','HOWARD','CAMPBELL',
        'SIMMONS','SANDERS','HENDERSON','COLLINS','COOPER','WATSON','BUTLER','ALEXANDER','BRYANT','NELSON','MORRIS',
        'BARNES','JORDAN','REED','WOODS','DIXON','ROBERTS','GRAY','PHILLIPS','GRIFFIN','BAKER','POWELL','BAILEY','FORD',
        'HOLMES','BANKS','DANIELS','ROSS','ROGERS','PERRY','FOSTER'];

    /**
     * @param $argv array - массив $argv
     * @throws Exception
     */
    public function __construct($argv)
    {
        $this->process_uid = $this->names[array_rand($this->names, 1)] . '_' .
            $this->last_names[array_rand($this->last_names, 1)] . '_' . rand(1000, 9999);

        if (!isset($argv[1])) {
            throw new Exception('Не указано имя очереди из которой необходимо извлекать сообщения');
        }
        $this->queue = $argv[1];

        if (!isset($argv[2])) {
            $this->host = 'localhost';
        } else {
            $this->host = $argv[2];
        }
    }

    private function init()
    {
        if (!isset($this->deficit_config)) {
            throw new Exception('Требуется задать значение параметра $deficit_config');
        }

        if (!file_exists($this->deficit_config)) {
            throw new Exception('Не существует конфигурационный файл по указанному пути');

        }
        require_once $this->deficit_config;

        if (!isset($this->vendor_dir)) {
            throw new Exception('Требуется задать значение параметра $vendor_dir');
        }

        if (!file_exists($this->vendor_dir . '/autoload.php')) {
            throw new Exception("Не существует файл {$this->vendor_dir}./autoload.php");
        }

        require_once $this->vendor_dir . '/autoload.php';

        if (!isset($this->log_dir)) {
            throw new Exception('Требуется задать значение параметра $log_dir');
        }

        if (!isset($this->app_dir)) {
            throw new Exception('Требуется задать значение параметра $app_dir');
        }

        if (!file_exists($this->log_dir)) {
            throw new Exception("Не существует каталога для логов");
        }

        if (!isset($config['components']['deficitQueue']['brokers'])) {
            throw new Exception("В конфигурационном файле отсутствует необходимая структура");
        }

        $config = $config['components']['deficitQueue']['brokers'];

        if (!isset($config[$this->host])) {
            throw new Exception("В конфигурационном файле отсутствует брокер с ключом {$this->host}");
        }

        $broker = $config[$this->host];

        $factory = new CentralDesktop\Stomp\ConnectionFactory\Failover([$broker['url']], true);
        $this->connection = new \CentralDesktop\Stomp\Connection($factory);
        $this->connection->connect($broker['login'], $broker['pass']);
        if (!$this->connection->subscribe($this->queue)) {
            throw new Exception("Не удалось подключиться к очереди {$this->queue}");
        }
    }

    public function setVendorDir($vendor_dir)
    {
        $this->vendor_dir = $vendor_dir;
    }

    public function setConfigFile($config_file)
    {
        $this->deficit_config = $config_file;
    }

    public function setLogDir($log_dir)
    {
        return $this->log_dir = $log_dir;
    }

    public function setAppDir($app_dir)
    {
        return $this->app_dir = $app_dir;
    }

    private function getMessage()
    {
        $frame = $this->connection->readFrame();
        if ($frame) {
            if ($frame->command == 'MESSAGE') {
                /*
                Сообщим ActiveMQ что мы получили сообщение сразу перед началом работы (ActiveMQ удалит его
                из очереди), чтобы в случае проблем в дальнейшем коде потребители больше не получали это сообщение,
                ибо будет грустно если из-за ошибки где-нибудь уйдёт например 10 000 запросов на оплату.
                 */
                $this->connection->ack($frame);

                $this->messages_count++;
                if ($this->messages_count % 1000 == 0) {
                    $this->checkMemory();
                    $this->messages_count = 0;
                }

                return json_decode($frame->body);
            }
        } else {
            return false;
        }
    }

    private function checkMemory()
    {
        if (file_exists("{$this->app_dir}/memory.deficit")) {
            $size = memory_get_usage(true);
            $memory = $size / 1024 . ' kb';
            $handler = fopen($this->log_dir . '/' . $this->process_uid, 'a');
            fwrite($handler, date("Y-m-d H:i:s") . ', ' . $this->queue . ': ' . $memory . "\n");
            fclose($handler);
        }

    }


    public function run()
    {
        $this->init();

        while (true) {
            if (file_exists("{$this->app_dir}/deficit.lock") || file_exists("{$this->app_dir}/{$this->queue}.lock")) {
                sleep(10);
                continue;
            }
            $message = $this->getMessage();
            if ($message) {
                $cmd = PHP_BINDIR . "/php {$this->app_dir}/yii $message->command $message->task_id";
                shell_exec($cmd);
            }
        }
    }
}

