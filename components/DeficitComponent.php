<?php

namespace chief88\deficit\components;

use chief88\deficit\exceptions\NoInjectionException;
use chief88\deficit\exceptions\NoRelatedException;
use chief88\deficit\exceptions\NoTaskException;
use chief88\deficit\exceptions\NoTaskTypeException;
use chief88\deficit\exceptions\StatusMismatchException;
use chief88\deficit\exceptions\StatusNotFoundException;
use chief88\deficit\exceptions\TaskAlreadySuccessfulException;
use chief88\deficit\exceptions\TaskPausedException;
use chief88\deficit\models\ExtendedTask;
use chief88\deficit\models\TaskPause;
use chief88\deficit\models\TaskPauseType;
use chief88\deficit\exceptions\NoTaskPauseTypeException;
use chief88\deficit\exceptions\NoSuchSettingException;
use Yii;
use yii\base\Component;
use chief88\deficit\models\TaskSettings;
use chief88\deficit\models\TaskStatus;
use chief88\deficit\models\TaskType;
use chief88\deficit\models\Task;
use yii\caching\TagDependency;
use yii\db\Transaction;

class DeficitComponent extends Component
{
    /** @var Task $task */
    private $task;

    /** @var $taskType TaskType */
    private $taskType;

    private $settings;

    private $injection;

    /** @var Transaction */
    private $art;

    public $default_support_email;

    public $isQueueDebug = false;

    const TRANSACTIONAL = 'transactional';

    const NOT_TRANSACTIONAL = 'not_transactional';

    const WITHOUT_SCHEDULER = null;

    const NO_RELATED_ID = 'no_related_id';

    /*
     * Общий тег для всех данных, если инвалидировать зависимость с этим тегом, то вообще весь кэш модуля сбросится.
     */
    const DEP_COMMON_TAG = 'deficit_cache_dependency_common_tag';
    /*
     * Тег который позволяет инвадировать кэш который относится к паузам конкретной задачи. Требуется конкантенировать
     * с id задачи справа.
     */
    const DEP_PAUSE_TAG_PREFIX = 'deficit_cache_dependency_pause_prefix_';

    const DEP_TASK_STATUS_TAG_PREFIX = 'deficit_cache_dependency_task_status_tag_prefix_';
    /*
     * самое длительное кэширование для долгосрочных данных
     * 60 * 60 * 24
     */
    const COMM_DURATION = 86400;
    /*
     * кэширование для пауз, которых может быть много, незачем забивать память
     * 60 * 5
     */
    const PAUSE_DURATION = 300;
    const STATUS_DURATION = 60;

    /**
     * Начинает транзакцию.
     *
     * Метод начинает сразу две транзакции - транзакцию базы данных и транзакцию ActiveMQ. Кроме обычных преимеществ
     * которые нам дают транзакции позволяет также существенно увеличить быстродействите. Этот метод используется
     * например при возврате задач в очередь в команде deficit/resender, при использовании транзакций скорость
     * работы команды увеличилась на порядок.
     * hash требуется для ActiveMQ чтобы определить какую из транзакций нужно коммитить или откатывать.
     * todo Подумать нужна ли поддержка работы с несколькими транзакциями
     *
     */
    public function beginTransaction()
    {
        $connection = \Yii::$app->db;
        $this->art = $connection->beginTransaction();

        /** @var Queue $deficitQueue */
        $deficitQueue = \Yii::$app->deficitQueue;
        $deficitQueue->beginTransaction();
    }

    /**
     * Коммит транзакций.
     *
     * Коммитит обе транзакции - и для базы данных и для ActiveMQ.
     */
    public function commitTransaction()
    {
        $this->art->commit();

        /** @var Queue $deficitQueue */
        $deficitQueue = \Yii::$app->deficitQueue;
        $deficitQueue->commitTransaction();
    }

    /**
     * Откат транзакций.
     *
     * Откатывает обе транзакции - и для базы данных и для ActiveMQ.
     */
    public function rollbackTransaction()
    {
        $this->art->rollBack();

        /** @var Queue $deficitQueue */
        $deficitQueue = \Yii::$app->deficitQueue;
        $deficitQueue->abortTransaction();
    }

    /**
     * Создаёт задачу и возвращает её объект.
     *
     * Вызов метода создаёт задачу и возвращает её объект. На этом этапе задача не отправлена в очередь и существует
     * только в базе данных в статусе "новая". Для того чтобы задача оказалась в очереди после вызова метода
     * createTask() нужно вызвать метод continueTask(), после вызова которого задача пройдёт по необходимым статусам
     * и отправится в очередь ActiveMQ.
     *
     * @param string $taskTypeSlug идентифицирует тип задачи к которому принадлежит задача, должно соответствовать
     * полю slug из таблицы task_type
     *
     * @param string|integer $relatedEntityId идентифицирует объект с которым будет работать задача, это обычно
     * primary key или любой другой идентификатор из таблицы содержащей обрабатываемые объекты, это может быть
     * строка таблицы с письмом, смс, платежом, персональными данными которые нужно обработать и т.п.
     *
     * @param null|string $description описывает создаваемую нами задачу, текст будет сохранён в поле description
     * таблицы task
     *
     * @param null|integer $parent_id ID родительской задачи
     *
     * @param array $data Дополнительные данные, которые будут сохранены как jsonb в таблицу task_data.
     * Если передан не пустой массив то задача будет создана с типом ExtendedTask.
     *
     * @return Task объект задачи
     *
     * @throws NoTaskTypeException
     */
    public function createTask($taskTypeSlug, $relatedEntityId, $description = null, $data = [], $parent_id = null)
    {
        $taskType = $this->findTaskType($taskTypeSlug);

        if(!isset($taskType)) {
            throw new NoTaskTypeException(
                "В базе данных не найден тип задач $taskTypeSlug, (slug в таблице task_type)"
            );
        }
        $firstStatus = $this->getFirstStatus($taskType);
        if (empty($data)) {
            $task = new Task();
        } else {
            $task = new ExtendedTask();
            $task->data = $data;
        }
        $task->status = $firstStatus->id;
        $task->type = $taskType->id;
        $task->related = $relatedEntityId;
        $task->description = $description;
        $task->parent_id = $parent_id;
        $task->save();
        $this->setTask($task);
        return $this->task;
    }

    /**
     * Создание и отправка задачи в очередь
     * Этот метод нужен для объединения двух методов, чтобы не делать в местах создания задачи двух вызовов и обойтись
     * только одним.
     *
     * @param $taskTypeSlug
     * @param $relatedEntityId
     * @param null $description
     * @param array $data
     * @param null|integer $parent_id ID родительской задачи
     * @return Task $task
     * @throws NoTaskTypeException
     */
    public function createAndSendTask($taskTypeSlug, $relatedEntityId, $description = null, $data = [], $parent_id = null)
    {
        $task = $this->createTask($taskTypeSlug, $relatedEntityId, $description, $data, $parent_id);
        $this->continueTask($task, self::NOT_TRANSACTIONAL);
        return $task;
    }

    /**
     * Увеличивает приоритет задачи.
     *
     * Данный метод использует поддержку приоритетов в ActiveMQ. Используется для того чтобы позволить некоторым
     * задачам в очереди обработаться быстрее других, даже если они попали в очередь позже. Это нужно например
     * в случае если у нас при создании задач есть точки входа с разными приоритетами, или если в очереди много
     * сообщений и задача обрабатывается в несколько этапов. Если не использовать приоритеты, это приведёт к тому что
     * задача которая попала в очередь первой и выполнила первый этап отработает второй этап только после того
     * все остальные задачи также пройдут первый этап. В большинстве случаев логичнее сократить время обработки задачи
     * и после выполнения каждого этапа увеличивать приоритет.
     * Дополнительная информаци о приоритетах: http://activemq.apache.org/how-can-i-support-priority-queues.html
     * Приоритеты изменяются в диапазоне от 0 до 9, приоритет по-умолчанию 4.
     *
     * @param Task $task объект задачи
     * @param null|integer $delta величина, на которую увеличиваем приоритет
     */
    public function increaseTaskPriority(Task $task, $delta = null)
    {
        if (!isset($delta)) {
            $task->priority++;
        } else {
            $task->priority = $task->priority + $delta;
        }
        $task->save();
    }

    /**
     * Возвращает для задачи самый первый статус.
     *
     * Самый первый статус - это статус который задача получает при создании методом createTask(), когда этот метод
     * возвращает задачу она имеет этот статус. Этот статус не связан ни с каким этапом обработки задачи.
     * После этого статуса начинаются следующие друг за другом статусы связанные с этапами задачи - "не в очереди",
     * "в очереди", "в работе" и т.д.
     *
     * @param TaskType $taskType объект типа задачи
     * @return TaskStatus объект статуса задачи
     * @throws \Exception
     */
    public function getFirstStatus(TaskType $taskType)
    {
        /*
         * Статусы в идеале не должны меняться, кэшируем надолго
         */
        $key = TaskStatus::CACHE_FIRST_BY_TYPE_ID_KEY . $taskType->id;
        $firstStatus = \Yii::$app->cache->get($key);
        if (!$firstStatus) {
            $firstStatus = TaskStatus::findOne(["task_type" => $taskType->id, "is_new" => true]);
            $dependency = new TagDependency(['tags' => self::DEP_COMMON_TAG]);

            \Yii::$app->cache->set($key, $firstStatus, self::COMM_DURATION, $dependency);
        }

        if (!isset($firstStatus)) throw new StatusNotFoundException(
            "Не найден первый статус для задачи типа $taskType->description"
        );
        return $firstStatus;
    }

    /**
     * Возвращает для объекта статуса следующий статус.
     *
     * Через внешний ключ к строке в той же таблице находит следующий статус и если такой существует возвращает такой.
     * Для последнего статуса и для статуса "ошибка" не вернёт ничего и выбросит исключение.
     *
     * @param TaskStatus $taskStatus объект статуса задачи
     * @return TaskStatus следующий после $taskStatus объекта статуса задачи
     * @throws StatusNotFoundException
     */
    public function getNextStatus(TaskStatus $taskStatus)
    {
        return $taskStatus->getNextStatus();
    }

    /**
     * Возвращает предыдущий статус задачи.
     *
     * Через внешний ключ находит предыдущий статус задачи.
     *
     * @param TaskStatus $taskStatus текущий статус задачи
     * @return TaskStatus предыдущий статус задачи
     * @throws StatusNotFoundException
     */
    public function getPreviousStatus(TaskStatus $taskStatus)
    {
        return $taskStatus->getPreviousStatus();
    }

    /**
     * Обновляет статус задачи на следующий.
     *
     * Выставляет задаче следующий статус, который будет получен через внешний ключ в ту же таблицу. Необходмо для
     * последовательного прохождения задачи через заданные этапы/статусы.
     *
     * @return TaskStatus следующий статус который в случае успеха был выставлен задаче
     * @throws \Exception
     */
    public function setNextStatus()
    {
        $nextStatus = $this->task->getTaskStatus()->getNextStatus();
        $this->task->status = $nextStatus->id;
        $this->task->save();

        return $nextStatus;
    }

    /**
     * Выставляет задаче предыдущий статус.
     *
     * В некоторых случаях, например когда выполнение этапа задачи не завершилось успехом (api не ответило по таймауту,
     * возвратило ошибку "попробуйте позднее" и т.п.) может потребоваться выполнить данный этап задачи повторно, для
     * этого необходимо вернуться по статусам назад в обратном порядке, этот метод и используется в данном случае.
     *
     * @param Task $task объект задачи с которой ведётся работа
     * @return TaskStatus объект статуса который был выставлен задаче
     * @throws \Exception
     */
    public function setPreviousStatus(Task $task)
    {
        $currentStatus = TaskStatus::findOne(["id" => $task->status]);
        $previousStatus = $currentStatus->getPreviousStatus();
        $task->status = $previousStatus->id;
        $task->save();

        return $previousStatus;
    }

    /**
     * Метод продолжает выполнение задачи вперёд по статусам.
     *
     * Данный метод используется чтобы отправить в очередь сообщение для задачи которая находится в статусе "новая"
     * или в в статусе "в работе" одного из этапов. Для обоих этих случаев в очереди нет никаких сообщений. И если
     * текущий статус не последний, то для обоих случаев ("новый" и "в работе") далее будет статус "не в очереди"
     * следующего этапа. Будет выставлен этот статус, затем будет совершена попытка отправить сообщение в очередь
     * и если она завершится успехом, то задаче будет выставлег статус "в очереди".
     *
     * @param Task $task объект задачи с которой ведётся работа
     * @param string $transactional указывает использовать ли транзакции в методе trySendToQueue() или транзакционность
     * обеспечивается на более высоком уровне
     * @param int $waiting время на которое требуется поместить задачу в планировщик
     * @return null этот метод ничего не возвращает
     * @throws \Exception
     */
    public function continueTask(
        Task $task,
        $transactional = self::TRANSACTIONAL,
        int $waiting = self::WITHOUT_SCHEDULER
    ) {
        $this->setTask($task);
        //выставим промежуточный статус, на случай если ActiveMQ недоступен
        //cron будет следить за такими зависшими задачами
        $nextStatus = $this->setNextStatus();

        /*
         * Проверим не последний ли это статус. Без следующего статуса только последний и статус ошибки.
         * Если это не ошибка, то это последний статус.
         */
        if ($nextStatus->next == null && $nextStatus->is_error == null) {
            return null;
        }
        $this->trySendToQueue($task, $waiting, $transactional);
    }

    /**
     * Повторить текущий этап выполнения задачи.
     *
     * В некоторых случаях, например когда выполнение этапа задачи не завершилось успехом (api не ответило по таймауту,
     * возвратило ошибку "попробуйте позднее" и т.п.) может потребоваться выполнить данный этап задачи повторно.
     * Данный метод реализует этот функционал. Задача возвращается в обратном порядке к статусу текущего этапа
     * "не в очереди" и совершается попытка отправить задачу в очередь.
     * todo мне кажется не очень изящным вызывать два раза подряд метод getPreviousStatus(), стоит обдумать это место
     *
     * @param Task $task объект задачи с которым ведётся работа
     *
     * @param null|integer $waiting количество времени в миллисекундах, на которое нужно отправить задачу в планировщик
     * перед тем как задача вернётся в очередь. Например мы знаем, что при получении ошибки которую мы получили
     * нет смысла делать повторную попытку ранее чем через 10 минут, а по-умолчанию пауза перед выполнение текущего
     * этапа 40 секунд. Поэтому мы явно задаём время для ожидания, 10 минут, это 10*60*1000 миллисекунд
     *
     * todo остался паршивый артефакт - exit в конце метода. нужно его выпилить и проверить что это ничего не ломает
     *
     * @throws \Exception
     */
    public function repeatTask(Task $task, $waiting = null)
    {
        $this->setTask($task);
        $currentStatus = $task->getTaskStatus();
        $previousStatus = $currentStatus->getPreviousStatus();
        $previousStatus = $previousStatus->getPreviousStatus();
        $task->repeat_counter++;
        $task->status = $previousStatus->id;
        $task->save();
        $this->trySendToQueue($task, $waiting);
    }

    /**
     * Метод "полный взад"
     *
     * Метод возник как ответ на логику работы шлюза Xplat где спустя несколько этапов после подтверждения разрешения
     * на оплату можно получить ответ, что на самом деле этого разрешения не было. Видимо Xplat не стесняясь выставляет
     * всем на вид свои внутренние костыли. Приходится скакать через статусы.
     *
     * todo это вообще не дело, так совершенно не годится, полная хрень. надо обобщить и сделать нормально без костылей
     *
     * @param Task $task объект задачи с которым ведётся работа
     * @throws \Exception
     */
    public function backwardTask(Task $task)
    {
        $this->setTask($task);
        $currentStatus = $task->getTaskStatus();

        /* статус "в очереди" вместо "в работе" */
        $previousStatus = $currentStatus->getPreviousStatus();

        /* статус "не в очереди" вместо "в очереди" */
        $previousStatus = $previousStatus->getPreviousStatus();

        /* статус "в работе" предыдущего этапа */
        $previousStatus = $previousStatus->getPreviousStatus();

        /* статус "в очереди" предыдущего этапа */
        $previousStatus = $previousStatus->getPreviousStatus();

        /* Ну нихера себе приключения, надо что-то с этим сделать */

        $task->status = $previousStatus->id;
        $task->save();
        $this->trySendToQueue($task, self::WITHOUT_SCHEDULER);
    }

    /**
     * Метод пытается отправить сообщение в очередь.
     *
     * Данный метод получает объект задачи со статусом "не в очереди" и пытается отправить в очередь.
     *
     * @param Task $task объект задачи с которой ведётся работа
     *
     * @param null|integer $waiting опциональный параметр, позволяющий задать период времени в миллисекундах на
     * который задача помещается в планировщик перед тем как отправиться в очередь. Этот параметр используется в том
     * случае если trySendToQueue() вызывается из метода repeatTask() в который был передан параметр ожидания
     *
     * @param string $transactional указывает использовать ли в данном методе транзакции. Если не использовать, то
     * предполагается что управление транзакциями производится на более высоком уровне.
     *
     * @throws \Exception
     */
    private function trySendToQueue(Task $task, $waiting = null, $transactional = self::TRANSACTIONAL)
    {
        //получим тип задачи, чтобы знать в какую очередь отправлять сообщение
        $taskType = $task->getTaskType();
        //получим текущий статус "в работе" чтобы узнать какой выставить таймаут для планировщика ActiveMQ
        $currentStatus = $task->getTaskStatus();

        if (!isset($waiting)) {
            $waiting = $currentStatus->waiting;
        }

        //сообщение в очереди это просто json с id задачи и
        //именем команды, которая должна быть выполнена для обработки текущего состояния
        $nextStatus = $currentStatus->getNextStatus();
        $message = [
            'task_id' => $task->id,
            'command' => $nextStatus->command,
            'uniq' => md5(time() . rand(1, 1000)),
        ];
        if (!isset($waiting) || $waiting == 0) {
            $amq_headers = [];
        } else {
            $amq_headers = ["AMQ_SCHEDULED_DELAY" => $waiting];
        }

        $amq_headers['priority'] = $task->priority;

        /* Если в типе задачи указано сохранять сообщения на жёстком диске, то сообщим о этом брокеру */
        if ($task->taskType->persist_messages === true) {
            $amq_headers['persistent'] = "true";
        }

        /*
         * Завернём это дело в транзакцию, чтобы консюмер не начал отрабатывать код раньше чем выставится следующий
         * статус
         */
        try {
            if ($transactional == self::TRANSACTIONAL) $this->beginTransaction();

            /** @var Queue $deficitQueue */
            $deficitQueue = \Yii::$app->deficitQueue;
            if ($deficitQueue->send($taskType->queue, json_encode($message), $amq_headers)) {
                //раз ActiveMQ доступен, выставляем соответствующий статус "в очереди"
                $this->setNextStatus();
            }

            if ($transactional == self::TRANSACTIONAL) $this->commitTransaction();
        } catch (\Exception $e) {
            Log::info(['desc' => 'Исключение при отправке в очередь', 'error' => strval($e->getMessage())]);
            Log::info(['desc' => 'Трейс ошибки', 'error' => strval($e->getTraceAsString())]);

            if ($transactional == self::TRANSACTIONAL) $this->rollbackTransaction();
            throw $e;
        }
    }

    /**
     * Получает компонент с логикой задачи.
     *
     * Данный метод получает и сохраняет в атрибуте дефицита компонент который реализует логику конкретной задачи.
     * Название компонента задаётся в поле injection таблицы task_type, при этом компонент с таким же
     * названием должен существовать в конфигурационных файлах yii.
     *
     * @param TaskStatus $taskStatus статус задачи в котором хранится информация о компоненте отвечающем за логику
     * @throws NoInjectionException
     * @throws \Exception
     */
    public function injectLogic(TaskStatus $taskStatus)
    {
        if (!isset($taskStatus->injection)) throw new NoInjectionException(
            'Не указан injection компонент для статуса задачи.' . PHP_EOL .
            'Данные по статусу: ' . PHP_EOL .
            '..id: ' . $taskStatus->id . PHP_EOL .
            '..task_type: ' . $taskStatus->task_type . PHP_EOL .
            '..slug: ' . $taskStatus->slug . PHP_EOL .
            '..command: ' . $taskStatus->command . PHP_EOL .
            '..title: ' . $taskStatus->title . PHP_EOL .
            '..description: ' . $taskStatus->description . PHP_EOL .
            '..previous: ' . $taskStatus->previous . PHP_EOL .
            '..next: ' . $taskStatus->next . PHP_EOL .
            '..injection: ' . $taskStatus->injection
        );

        $injection_name = $taskStatus->injection;
        $injection = \Yii::$app->$injection_name;

        if (!isset($injection)) {
            throw new NoInjectionException(
                'Нет injection компонента "' . $injection_name . '" для статуса с ID ' . $taskStatus->id
            );
        }

        $this->injection = $injection;
    }

    /**
     * Возвращает компонент логики задачи.
     *
     * Данный метод возвращает компонент который описывает логику конкретной задачи, специфические для неё алгоритмы и
     * методы.
     *
     * @return mixed
     * @throws NoInjectionException
     */
    public function getInjection()
    {
        if (!isset($this->injection)) throw new NoInjectionException("Нет injection компонента");
        return $this->injection;
    }

    /**
     * @param $taskId integer номер задачи. (Подробнее в описании beginCommonWorking())
     * @param null $cmdName имя команды которая выполняется в текущий момент. (Подробнее в описании beginCommonWorking())
     * @return bool|Task
     * @throws TaskAlreadySuccessfulException
     * @throws TaskPausedException
     * @throws \Exception
     */
    public function beginWorking($taskId, $cmdName = null)
    {
        return $this->beginCommonWorking($taskId, $cmdName, Task::SCENARIO_BEGIN_WORKING);
    }

    /**
     * @param $taskId integer номер задачи. (Подробнее в описании beginCommonWorking())
     * @return bool|Task
     * @throws TaskAlreadySuccessfulException
     * @throws TaskPausedException
     * @throws \Exception
     */
    public function beginEasyWorking($taskId)
    {
        return $this->beginCommonWorking($taskId, null, Task::SCENARIO_EASY_WORKING);
    }

    /**
     * Метод начала работы с задачей.
     *
     * Используется методами beginWorking() и beginEasyWorking(). Методы отличаются дополнительной проверкой какой
     * этап работы в текущий момент производится. Если задача ответственная, связанная например с деньгами, то стоит
     * использовать beginWorking() чтобы например не было даже минимального риска выполнить два раза эта отправки
     * запроса на оплату.
     * Если задача не имеет таких критических моментов, то проще использовать beginEasyWorking().
     *
     * Этот методы вызывается в консольной команде в самом начале работы этапа задачи. Этот метод производит
     * необходимые проверки и переводит задачу в статус "в работе".
     *
     * @param $taskId integer primary key из таблицы task, один из двух параметров который отправляется как сообщение
     * ActiveMQ
     *
     * @param null|string $cmdName имя команды которая выполняется в текущий момент. Это второй параметр сообщения
     * ActiveMQ. Данный параметр жёстко прописывается в консольной команде для обнаружения несоотвествий между
     * текущим статусом и тем который ожидается в тот момент когда консюмер получает сообщение и начинает выполнять
     * консольную команду. (Вышесказанное справедливо только для случая начала работы с помощью метода beginWorking(),
     * при работе метода beginEasyWorking() эти проверки не производятся)
     *
     * @param string $scenario тип используемого сценария для валидации, от него зависит будет ли осуществляться проверка
     * на этап задачи с помощью $cmdName или не будет.
     *
     * @return bool|Task если на момент получения сообщения из очереди есть активные паузы, или если задача уже успешно
     * завершена, то возвращается false, иначе возвращается объект задачи с которой ведётся работа
     *
     * @throws NoTaskException
     * @throws TaskAlreadySuccessfulException
     * @throws StatusMismatchException
     * @throws TaskPausedException
     * @throws NoRelatedException
     * @throws \Exception
     */
    private function beginCommonWorking($taskId, $cmdName, $scenario)
    {
        try {
            $this->beginTransaction();

            /** @var Task $task */
            $task = Task::findBySql("SELECT * FROM task WHERE task.id=:id FOR UPDATE ", ['id' => $taskId])
                ->one();

            if ($task === null) throw new NoTaskException("Не удалось начать работу, нет задачи с id '$taskId'");
            $this->setTask($task);

            /*
             * Сохраним в модели имя команды которое мы получили из контроллера консольной команды, нужно для
             * валидатора, чтобы проверить, что задача в корректном статусе.
             */
            $task->commandName = $cmdName;
            $task->scenario = $scenario;

            $task->validate();

            /*
             * Ставим сценарий по-умолчанию потому что позднее есть вызов setNextStatus() и со сценарием 'begin_working'
             * при сохранении модели валидация не будет пройдена. А полагаться только на валидацию при сохранении нельзя,
             * потому как до него будет тот же setNextStatus() которого может и не быть.
             */
            $task->scenario = Task::SCENARIO_DEFAULT;

            $this->injectLogic($task->taskStatus);
            //получим настройки для типа задачи
            $this->setSettings($task->taskType);
            $this->setNextStatus();

            $this->commitTransaction();

            return $task;

        } catch (TaskAlreadySuccessfulException $e) {
            /*
             * Это допустимо и нормально, ничего не делаем. Выбросим обратно для ясности.
             */
            $this->rollbackTransaction();
            throw $e;
        } catch (TaskPausedException $e) {
            /*
             * Это допустимо и нормально, ничего не делаем. Выбросим обратно для ясности.
             */
            $this->rollbackTransaction();
            throw $e;
        } catch (\Exception $e) {
            /*
             * А тут что-то уже не нормальное, залогируем.
             */
            \Yii::error(
                "beginWorking, taskId: {$taskId}, Message: ".$e->getMessage()."\n"."File: ".$e->getFile()."\n"."Line: ".
                $e->getLine()."\n"."Trace: ".$e->getTraceAsString(), 'deficit_task_exception'
            );

            $this->rollbackTransaction();
            throw $e;
        }
    }

    /**
     * Сохранить в компоненте настройки задачи.
     *
     * Извлекает из таблицы task_settings настройки связанные с задачей и сохраняет их в атрибуте компонента.
     *
     * @param TaskType $taskType объект типа задачи с которой ведётся работа.
     */
    public function setSettings(TaskType $taskType)
    {
        //получим настройки для типа задачи
        /*
         * Настройки должны меняться редко, кэшируем надолго
         */
        $key = TaskSettings::CACHE_BY_TYPE_KEY . $taskType->id;
        $_settings = \Yii::$app->cache->get($key);
        if (!$_settings) {
            $_settings = TaskSettings::find()->where(['task_type' => $taskType->id])->all();
            $dependency = new TagDependency(['tags' => [self::DEP_COMMON_TAG]]);
            \Yii::$app->cache->set($key, $_settings, self::COMM_DURATION, $dependency);
        }

        $settings = [];
        foreach ($_settings as $setting)
            $settings[$setting->slug] = $setting->value;
        $this->settings = $settings;
    }

    /**
     * Возвращает настройку для задачи.
     *
     * Возвращает настройку связанную с задачей из числа тех что ранее были получены методом setSettings()
     * В настройках предполагается хранить логины, пароли, адреса для запросов по api и т.д. и т.п.
     *
     * @param string $key ключ по которому идентифицируется параметр
     * @return mixed
     * @throws \Exception
     */
    public function getSetting($key)
    {
        if (!isset($this->settings[$key])) {
            throw new NoSuchSettingException("Параметра с ключом $key не существует");
        }
        return $this->settings[$key];
    }

    /**
     * Возвращает статус "ошибка" для типа задачи.
     *
     * Находит в базе данных статус который соответствует для данного типа задачи ошибке.
     *
     * @param TaskType $taskType объект типа задачи с которой ведётся работа
     * @return null|TaskStatus объект статуса
     */
    public function getErrorStatus(TaskType $taskType)
    {
        /*
         * Статусы в идеале не должны меняться, кэшируем надолго
         * Ok
         */
        $key = TaskStatus::CACHE_ERROR_BY_TYPE_ID_KEY . $taskType->id;
        $errorStatus = \Yii::$app->cache->get($key);
        if (!$errorStatus) {
            $errorStatus = TaskStatus::findOne(['task_type' => $taskType->id, 'is_error' => true]);
            $dependency = new TagDependency(['tags' => self::DEP_COMMON_TAG]);
            \Yii::$app->cache->set($key, $errorStatus, self::COMM_DURATION, $dependency);
        }
        return $errorStatus;
    }

    /**
     * Перевести задачу в ошибку.
     *
     * Находит статус ошибки для задачи и выставляет этот статус задаче. В статус "ошибка" задача выставляется только
     * в том случае, если произошла ошибка где-то в коде модуля или компонента реализующего логику задачи. Статус
     * задачи "ошибка" не должен никак связываться с ошибками выполнения задачи. Например если платёжный шлюз на
     * попытку совершить оплату ответил сообщением "ваш платёж отклонён из-за ошибки" то в этой ситуации статус "ошибка"
     * выставляется только платежу, а связанная с ним задача выставляется в статус "успешно завершена", ведь в нашем
     * коде не было никаких ошибок, не возникали исключения и всё сработало согласно предусмотренной нами логике.
     * Если наш код написан корректно и без ошибок то в статус "ошибка" задача не попадёт НИКОГДА.
     *
     * @param Task $task объект задачи с которой ведётся работа.
     * @param \Exception $exception
     * @throws NoTaskTypeException
     */
    public function task2Error(Task $task, \Exception $exception = null)
    {
        if (isset($exception))
            \Yii::error(
                "task2Error, Message: ".$exception->getMessage()."\n"."File: ".$exception->getFile()."\n"."Line: ".
                $exception->getLine()."\n"."Trace: ".$exception->getTraceAsString(), 'deficit_task_exception'
            );

        $taskType = TaskType::findOne(["id" => $task->type]);
        if (!isset($taskType)) {
            throw new NoTaskTypeException(
                "В базе данных не найден тип задач с id $task->type, (slug в таблице task_type)"
            );
        }
        $errorStatus = $this->getErrorStatus($taskType);
        $task->status = $errorStatus->id;
        $task->save();
    }

    /**
     * Возвращает статус "успешно завершено" для задачи.
     *
     * Находит и возвращает статус который выставляется задаче по успешному завершению задачи.
     *
     * @param TaskType $taskType объект типа задачи с которой ведётся работа
     * @return null|TaskStatus
     */
    public function getSuccessStatus(TaskType $taskType)
    {
        /*
         * Статусы меняются редко, в идеале вообще не меняются, кэшируем надолго
         * Ok
         */
        $key = TaskStatus::CACHE_SUCCESS_BY_TYPE_ID_KEY . $taskType->id;
        $successStatus = \Yii::$app->cache->get($key);
        if (!$successStatus) {
            $successStatus = TaskStatus::findOne(['task_type' => $taskType->id, 'is_error' => null, 'next' => null]);
            $dependency = new TagDependency(['tags' => self::DEP_COMMON_TAG]);
            \Yii::$app->cache->set($key, $successStatus, self::COMM_DURATION, $dependency);
        }
        return $successStatus;
    }

    /**
     * Выставление задаче статуса "успешно завершена".
     *
     * При нормальном процессе отработки задачи данный метод не используется, потому как постоянный вызов continueTask()
     * приводит задачу к логическому завершению - статусу "успешно завершена". Но если например задача теряет
     * актуальность, или по любой другой причине продолжать её выполнение нет смысла, то тогда задача досрочно
     * выставляется в статус "успешно завершена".
     *
     * @param Task $task
     * @throws NoTaskTypeException
     */
    public function task2Success(Task $task)
    {
        if (!isset($task->taskType)) {
            throw new NoTaskTypeException(
                "В базе данных не найден тип задач с id $task->type, (slug в таблице task_type)"
            );
        }
        $successStatus = $this->getSuccessStatus($task->taskType);
        $task->status = $successStatus->id;
        $task->save();
    }

    /**
     * Сохраняет задачу в атрибут компонента.
     *
     * Получает и сохраняет задачу в атрибут компонента для дальнейшего удобного доступа к ней.
     *
     * @param Task $task объект задачи с которым ведётся работа
     */
    public function setTask(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Получает объект задачи.
     *
     * Получаем объект задачи который ранее сохранили в компонент с помощью setTask()
     *
     * @return Task объект задачи с которой ведётся работа
     * @throws NoTaskException
     */
    public function getTask()
    {
        if (!isset($this->task))
            throw new NoTaskException("Не могу вернуть сущность задачи, так как задача не была задана ранее");
        return $this->task;
    }

    /**
     * Сеттер для типа задачи.
     *
     * Возникла необходимость сохранять тип задачи в компоненте для тех случаев, когда например по крону выполняется
     * какая-то команда, которой нужен метод из компонента типа задачи, например запрос баланса. В такие моменты
     * никакой задачи может не быть, связать запрос не с чем, а данные логировать надо. Поэтому будем связывать такие
     * данные логов не с задачей, а с типом задачи.
     *
     * @param TaskType $taskType
     */
    public function setTaskType(TaskType $taskType)
    {
        $this->taskType = $taskType;
    }

    /**
     * Геттер для типа задачи.
     *
     * @return TaskType
     * @throws NoTaskTypeException
     */
    public function getTaskType()
    {
        if(!isset($this->taskType)) {
            throw new NoTaskTypeException('Не могу вернуть тип задачи так как он не был задан ранее');
        }
        return $this->taskType;
    }

    /**
     * Приостановка задачи.
     *
     * Метод создаёт запись в таблице task_pause, которая в столбце is_active содержит истину (когда пауза
     * становится не актуальной, флаг true меняется на false). После того как задача выствлена в паузу, её статус
     * меняется на "не в очереди". Сообщение из очереди поступает к консюмеру, но из-за паузы никаких действий не
     * производится. Чтобы возобновить выполнение задачи нужно снять её с паузы с помощью метода resumeOneTask() или
     * метода resumeTasks() (снимает с паузы определённого типа все задачи некоторого типа, например снимает с паузы
     * все задачи по оплате через какой-нибудь шлюз, которые ранее все были поставлены в паузу из-за нехватки денег)
     *
     * @param $pauseTypeSlug string соответствующая полю slug из таблицы task_pause_type
     * @return TaskPause объект паузы которая была выставлена задаче
     * @throws \Exception
     */
    public function pauseTask($pauseTypeSlug)
    {
        if(!isset($this->task)) {
            throw new NoTaskException("Не удаётся поставить задачу в паузу, задача не задана методом setTask()");
        }
        $taskPauseType = $this->getPauseType($pauseTypeSlug, $this->task->taskType->slug);

        $pause = new TaskPause();
        $pause->pause_type = $taskPauseType->id;
        $pause->is_active = true;
        $pause->task = $this->task->id;
        $pause->save();

        $currentStatus = $this->task->getTaskStatus();
        $previousStatus = $currentStatus->getPreviousStatus();
        $previousStatus = $previousStatus->getPreviousStatus();
        $this->task->status = $previousStatus->id;
        $this->task->save();

        TagDependency::invalidate(Yii::$app->cache, self::DEP_PAUSE_TAG_PREFIX . $this->task->id);

        return $pause;
    }

    /**
     * Получает тип паузы.
     *
     * Находит по слагу типа паузы и слагу типа задачи объект типа паузы для этого типа задачи.
     *
     * @param $pauseTypeSlug string слаг типа паузы
     * @param $taskTypeSlug string слаг типа задачи
     * @return TaskPauseType объект искомого типа паузы
     * @throws \Exception
     * @throws NoTaskPauseTypeException
     */
    public function getPauseType($pauseTypeSlug, $taskTypeSlug)
    {
        $taskType = $this->findTaskType($taskTypeSlug);

        /*
         * Типы пауз меняются редко, в идеале вообще не меняются, кэшируем надолго
         * Ok
         */
        $key = TaskPauseType::CACHE_KEY . $pauseTypeSlug . $taskTypeSlug;
        $taskPauseType = \Yii::$app->cache->get($key);

        if (!$taskPauseType) {
            $taskPauseType = TaskPauseType::findAll(['slug' => $pauseTypeSlug, 'task_type' => $taskType->id]);
            $dependency = new TagDependency(['tags' => self::DEP_COMMON_TAG]);
            \Yii::$app->cache->set($key, $taskPauseType, self::COMM_DURATION, $dependency);
        }

        if (sizeof($taskPauseType) > 1) {
            throw new \Exception(
                "Найдено больше одного типа паузы со slug $pauseTypeSlug для типа задач с id $taskType->id"
            );
        }
        if (sizeof($taskPauseType) == 0) {
            throw new NoTaskPauseTypeException(
                "Не найден типа паузы со slug $pauseTypeSlug для типа задач с id $taskType->id"
            );
        }

        return $taskPauseType[0];
    }

    /**
     * Снять с паузы одну задачу
     *
     * Снимает с паузы одну-единственную задачу, в отличии от метода resumeTasks() которые снимает с паузы все задачи
     * определённого типа и определённого типа паузы.
     *
     * @param TaskPause $taskPause Объект паузы, прикрепленный к задаче, которую нужно снять с паузы
     * @throws \Exception
     * @return boolean
     */
    public function resumeOneTask(TaskPause $taskPause)
    {
        $task = $taskPause->relatedTask;
        $this->setTask($task);

        $taskPause->is_active = false;
        $result = $taskPause->save();
        if (!$result) {
            $errors = $taskPause->getErrors();
        }
        TagDependency::invalidate(Yii::$app->cache, self::DEP_PAUSE_TAG_PREFIX . $this->task->id);
        return true;
    }

    /**
     * Возвращает объект типа задачи.
     *
     * Находит по слагу типа задачи объект этого типа и возвращает его.
     *
     * @param $taskTypeSlug string
     * @return TaskType - объект типа задачи
     * @throws NoTaskTypeException
     */
    public function findTaskType($taskTypeSlug)
    {
        /*
         * Типы задач меняются редко, в идеале не меняются, кэшируем надолго
         * Ok
         */
        $key = TaskType::CACHE_BY_SLUG_KEY . $taskTypeSlug;
        $taskType = \Yii::$app->cache->get($key);
        if (!$taskType) {
            $taskType = TaskType::findOne(["slug" => $taskTypeSlug]);
            $dependency = new TagDependency(['tags' => self::DEP_COMMON_TAG]);
            \Yii::$app->cache->set($key, $taskType, self::COMM_DURATION, $dependency);
        }

        if (!isset($taskType)) throw new NoTaskTypeException("Не удалось найти тип задач со slug $taskTypeSlug");
        return $taskType;
    }

    /**
     * Снимает с пазы определённого типа задачи определённого типа.
     *
     * Находит задачи некотором типа, определяемого слагом, находящиеся в паузе определённого типа, также задаётся
     * слагом типа паузы. И снимает для этих пауз флаг активности, после чего команда переотправки задач в очередь
     * может найти эти задачи и вернуть их в работу.
     *
     * @param $taskPauseTypeSlug string слаг типа паузы
     * @param $taskTypeSlug string слаг типа задачи
     * @return int - количество задач которое было снято с паузы
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function resumeTasks($taskPauseTypeSlug, $taskTypeSlug)
    {
        $pauses = TaskPause::find()
            ->with("relatedTask")
            ->innerJoin("task_pause_type", "task_pause.pause_type = task_pause_type.id")
            ->innerJoin("task_type", "task_pause_type.task_type = task_type.id")
            ->andWhere(["task_type.slug" => $taskTypeSlug])
            ->andWhere(["task_pause_type.slug" => $taskPauseTypeSlug])
            ->andWhere(["task_pause.is_active" => TRUE])
            ->all();
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            /** @var TaskPause $pause */
            foreach ($pauses as $pause) {
                $pause->is_active = false;
                $pause->save();
                TagDependency::invalidate(Yii::$app->cache, self::DEP_PAUSE_TAG_PREFIX . $pause->relatedTask->id);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            echo $e->getMessage();
            echo "\nОткатываем транзакцию\n";
        }
        return count($pauses);
    }

    /**
     * Переотправляет задачи в очередь.
     *
     * Находит все задачи находящимеся в статусе "не в очереди" и не имеющие активных пауз и делает попытку
     * переотправить их в очередь.
     * хороший пример тут: http://stackoverflow.com/questions/14710632/mysql-excluding-several-rows-if-one-joined-row-matches-a-condition
     * и немного общей информации тут: http://stackoverflow.com/questions/4560471/how-to-exclude-rows-that-dont-join-with-another-table
     *
     * @return int количество задач которое было переотправлено в очередь
     */
    public function resendTasksToQueue()
    {
//        $max_id = Task::findBySql("SELECT * FROM task WHERE task.updated=(now() - task.updated) > '00:01:00'")
//            ->one();

        $now = new \DateTime('now');
        $oneMinuteAgo = $now->modify('-1 minute');
        $model = Yii::$app->db->createCommand("SELECT min(id) FROM task WHERE task.updated>:updated",
            ['updated' => $oneMinuteAgo->format('Y-m-d H:i:s')]
        );
        $min_id = $model->queryOne();

        $query = Task::find()
            ->leftJoin('task_pause', 'task_pause.task = task.id and task_pause.is_active is true')
            ->andWhere('task.is_unqueued IS TRUE AND
                        task.is_success IS FALSE AND
                        task.is_error IS FALSE AND
                        task_pause.task is NULL');

        if (!is_null($min_id['min'])) {
            $query->andWhere('task.id < :max_id', ['max_id' => $min_id['min']]);
        }

        $tasks = $query->all();

        try {
            $this->beginTransaction();
            foreach ($tasks as $task) {
                //методы ниже ожидают, что в атрибуте компонента будет указана текущая задача
                $this->setTask($task);
                //отправляем задачу в очередь
                $this->trySendToQueue($task, self::WITHOUT_SCHEDULER, self::NOT_TRANSACTIONAL);
            }
            $this->commitTransaction();
        } catch (\Exception $e) {
            $this->rollbackTransaction();
        }

        return sizeof($tasks);
    }

    /**
     * Поиск задачи по id связанного с ней объекта.
     *
     * Находит запись из таблицы task используя slug из таблицы task_type и id находящийся в таблице
     * task в поле related.
     * todo переработать этот метод, потому как возможна ситуация когда один объект будет необходимо обработать
     * todo несколько раз и тогда в поле related будет один и тот же id и это вызовет проблемы
     *
     * @param $taskTypeSlug string слаг типа задачи
     * @param $relatedId integer id объекта связанного с задачей
     * @return Task объект задачи связанный с объект хранящим информацию о задаче
     * @throws \Exception
     * @throws NoTaskException
     */
    public function getTaskByRelated($taskTypeSlug, $relatedId)
    {
        $taskType = $this->findTaskType($taskTypeSlug);
//        $task = Task::findAll(['type' => $taskType->id, 'related' => $relatedId]);
        $task = Task::find()->select('task.*')
            ->leftJoin('task_status', 'task.status = task_status.id')
            ->where('task_status.is_success is not true and task.type = :type_id and task.related = :related_id',
                [
                    'type_id' => $taskType->id,
                    'related_id' => $relatedId,
                ])->one();
        if (!isset($task)) throw new NoTaskException("Не удалось найти задачу с relatedId: $relatedId, и с типом $taskTypeSlug.");
        $this->setTask($task);
        return $task;
    }

    /**
     * Метод создающий структуру задачи в БД.
     *
     * Метод по массиву создаёт структуру с типом задачи, статусами задачи, паузами задачи и настройками задачи.
     *
     * @param $data array массив с информацией описывающей структуру задачи
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    static function createDbEnvironment($data)
    {
        TypeEditor::createDbEnvironment($data);
    }

    /**
     * Метод удаление из базы данных структуры задачи.
     *
     * По слагу находит структуру задачи в базе данных и удаляет её.
     *
     * @param $taskTypeSlug string слаг задачи которую нужно удалить из базы данных
     * @throws \Exception
     */
    static function rollbackDbEnvironment($taskTypeSlug)
    {
        TypeEditor::rollbackDbEnvironment($taskTypeSlug);
    }
}
