<?php

namespace chief88\deficit\components;

use chief88\deficit\exceptions\NoTaskException;
use chief88\deficit\models\Task;
use chief88\deficit\models\TaskType;
use Psr\Log\NullLogger;
use yii\base\Component;
use CentralDesktop\Stomp\Connection as Stomp;
use CentralDesktop\Stomp\ConnectionFactory;
use chief88\deficit\services\CentraldesktopLogger;

class Queue extends Component
{
    public $brokers;
    private $connections = [];
    private $amq_transaction_hash = [];
    private $subscribed_broker = null;

    /**
     * Инициализация компонента.
     *
     * @throws \Exception
     */
    public function init()
    {
        parent::init();

        foreach ($this->brokers as $name => $broker) {
            try {
                $factory = new ConnectionFactory\Failover([$broker['url']], true);
                $this->connections[$name] = new Stomp($factory);
                $this->connections[$name]->connect($broker['login'], $broker['pass']);
            } catch (\Exception $e) {
                $this->connections[$name] = null;
            }
        }
    }

    public function subscribe($queue, $broker_name)
    {
        $this->subscribed_broker = $broker_name;
        $this->connections[$this->subscribed_broker]->subscribe($queue);
    }

    public function getMessage()
    {
        $random_id = rand(1000,9999) . '-' . rand(1000,9999);
        if (\Yii::$app->deficitComponent->isQueueDebug) {
            $logger = new CentraldesktopLogger();
            $this->connections[$this->subscribed_broker]->setLogger($logger);
            \Yii::info(
                "READING=begin=$random_id======================================================",
                'amq_debug'
            );
        }

        $frame = $this->connections[$this->subscribed_broker]->readFrame();

        if ($frame) {
            if ($frame->command == 'MESSAGE') {
                /*
                Сообщим ActiveMQ что мы получили сообщение сразу перед началом работы (ActiveMQ удалит его
                из очереди), чтобы в случае проблем в дальнейшем коде потребители больше не получали это сообщение,
                ибо будет грустно если из-за ошибки где-нибудь уйдёт например 10 000 запросов на оплату.
                 */
                $this->connections[$this->subscribed_broker]->ack($frame);

                if (\Yii::$app->deficitComponent->isQueueDebug) {
                    \Yii::info(
                        "READING=end=$random_id========================================================",
                        'amq_debug'
                    );
                    $logger = new NullLogger();
                    $this->connections[$this->subscribed_broker]->setLogger($logger);
                }

                return json_decode($frame->body);
            }
        } else {
            if (\Yii::$app->deficitComponent->isQueueDebug) {
                \Yii::info(
                    "READING=end=$random_id========================================================",
                    'amq_debug'
                );
                $logger = new NullLogger();
                $this->connections[$this->subscribed_broker]->setLogger($logger);
            }
            return false;
        }
    }


    private function getBrokerNames()
    {
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        try {
            /** @var Task $task */
            $task = $deficit->getTask();
            /** @var TaskType $taskType */
            $taskType = $task->getTaskType();
        } catch (NoTaskException $e) {
            $taskType = $deficit->getTaskType();
        }

        $names = json_decode($taskType->broker_name, true);
        foreach ($names as $name => $weight) {
            if (!isset($this->brokers[$name])) {
                throw new \Exception("Брокера с ключом {$name} нет в конфигурационном файле");
            }
        }
        return $names;
    }

    /**
     * Отправка сообщения в очередь.
     *
     * Обёртка для метода отправки сообщений в очередь предоставленного библиотекой centraldesktop/stomp, в самой
     * библиотеке fwrite может выбрасывать эксепшоны, которые никак там не перехватываются, этот факт взывает к
     * написанию обёртки которая это всё же делала бы.
     *
     * @param $queue string имя очереди
     * @param $message string сообщение которое отправляется в очередь
     * @param $amq_headers array заголовки ActiveMQ
     * @param $is_log_message boolean флаг является ли сообщение лог-сообщением. Для лог сообщений крайне нежелательна
     * транзакционность, логи должны быть всегда.
     *
     * @return bool удалось ли отправить сообщение
     */
    public function send($queue, $message, $amq_headers = [], $is_log_message = false)
    {
        $broker_names = $this->getBrokerNames();
        $for_array_random = [];
        foreach ($broker_names as $name => $weight) {
            $for_array_random = array_merge($for_array_random, array_fill(0, $weight, $name));
        }
        $broker_name = $for_array_random[array_rand($for_array_random)];

        if (isset($this->amq_transaction_hash[$broker_name]) && !$is_log_message) {
            $amq_headers['transaction'] = $this->amq_transaction_hash[$broker_name];
        }

        if (isset($this->connections[$broker_name])) {
            try {
                if (!$is_log_message && \Yii::$app->deficitComponent->isQueueDebug) {
                    $logger = new CentraldesktopLogger();
                    $this->connections[$name]->setLogger($logger);
                    $message_array = json_decode($message, true);
                    \Yii::info(
                        "SENDING=begin=task=={$message_array['task_id']}==uniq:{$message_array['uniq']}=",
                        'amq_debug'
                    );
                }

                $result = $this->connections[$broker_name]->send($queue, $message, $amq_headers, true);
                if (!$is_log_message && \Yii::$app->deficitComponent->isQueueDebug) {
                    \Yii::info(
                        "SENDING=end=task===={$message_array['task_id']}================================",
                        'amq_debug'
                    );
                    $logger = new NullLogger();
                    $this->connections[$name]->setLogger($logger);
                }
                return $result;
            } catch (\Exception $e) {
                $log_msg = [
                    'desc' => 'Исключение при отправке сообщения',
                    'message' => $e->getMessage()
                ];

                Log::log2Db($log_msg);
            }

        } else {
            return false;
        }
    }

    /**
     * Начало транзакции
     *
     * Начинает ActiveMQ транзакцию, создаёт хэш, по которому идентифицируется транзакция.
     */
    public function beginTransaction()
    {
        foreach ($this->connections as $name => $connection) {
            if (is_null($connection))
                continue;
            $hash = md5(time() . rand(1, 10000));
            $this->amq_transaction_hash[$name] = $hash;
            $connection->begin($hash, true);
        }
    }

    /**
     * Коммит транзакции
     */
    public function commitTransaction()
    {
        foreach ($this->connections as $name => $connection) {
            if (is_null($connection))
                continue;
            $connection->commit($this->amq_transaction_hash[$name], true);
            $this->amq_transaction_hash[$name] = null;
        }
    }

    /**
     * Отмена транзакции
     */
    public function abortTransaction()
    {
        foreach ($this->connections as $name => $connection) {
            if (is_null($connection))
                continue;
            $connection->abort($this->amq_transaction_hash[$name], true);
            $this->amq_transaction_hash[$name] = null;
        }
    }
}
