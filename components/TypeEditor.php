<?php

namespace chief88\deficit\components;

use chief88\deficit\models\Task;
use chief88\deficit\models\TaskData;
use chief88\deficit\models\TaskPauseType;
use chief88\deficit\models\TaskSettings;
use chief88\deficit\models\TaskStatus;
use chief88\deficit\models\TaskType;
use yii\base\Component;
use yii\helpers\BaseConsole;

class TypeEditor extends Component
{
    /**
     * Метод создающий структуру задачи в БД.
     *
     * Метод по массиву создаёт структуру с типом задачи, статусами задачи, паузами задачи и настройками задачи.
     *
     * @param $data array массив с информацией описывающей структуру задачи
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public static function createDbEnvironment($data)
    {
        if (!isset($data['task_type'])) {
            throw new \Exception("Отсутствует элемент описывающий тип задачи");
        }

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $task_type = new TaskType();
            $task_type->slug = $data['task_type']['slug'];
            $task_type->title = $data['task_type']['title'];
            $task_type->description = $data['task_type']['description'];
            $task_type->queue = $data['task_type']['queue'];
            /*
             * Немного легаси
             * todo придумать что с этим делать
             */
            $task_type->injection = $data['task_type']['injection'];
            $task_type->log_group = $data['task_type']['log_group'];
            $task_type->persist_messages = $data['task_type']['persist_messages'];
            if (isset($data['task_type']['broker_name'])) {
                $task_type->broker_name = $data['task_type']['broker_name'];
            } else {
                $task_type->broker_name = json_encode(['localhost' => 1]);
            }
            $task_type->save();

            $status = new TaskStatus();
            $status->slug = "new";
            $status->title = "Новая задача";
            $status->description = "Первоначальный статус присваиваемый задаче до отправки в очередь";
            $status->task_type = $task_type->id;
            $status->is_new = true;
            $status->save();
            $status_previous = $status;

            foreach ($data['statuses'] as $data_status) {
                if (!isset($data_status['waiting'])) {
                    $data_status['waiting'] = null;
                }

//                if(!isset($data_status['injection'])){
//                    throw new \Exception('Не указан injection компонент для данного статуса');
//                }

                $status = new TaskStatus();
                $status->slug = $data_status['slug']."_unqueued";
                $status->title = $data_status['title'].", НЕ в очереди";
                $status->description = $data_status['description'].", задача ещё не в очереди";
                $status->task_type = $task_type->id;
                $status->is_unqueued = true;
                $status->waiting = $data_status['waiting'];
                $status->next = '';
                $status->previous = $status_previous->id;
                $status->save();
                $status_previous->next = $status->id;
                $status_previous->save();

                $status_previous = $status;

                $status = new TaskStatus();
                $status->slug = $data_status['slug'];
                $status->title = $data_status['title'];
                $status->description = $data_status['description'];
                $status->task_type = $task_type->id;
                $status->next = '';
                $status->command = $data_status['command'];
                $status->previous = $status_previous->id;
                if (isset($data_status['injection'])) {
                    $status->injection = $data_status['injection'];
                }
                $status->save();
                $status_previous->next = $status->id;
                $status_previous->save();

                $status_previous = $status;

                $status = new TaskStatus();
                $status->slug = $data_status['slug']."_working";
                $status->title = $data_status['title'].", ведётся работа";
                $status->description = $data_status['description'].", задача извлечена из очереди, ведётся работа";
                $status->task_type = $task_type->id;
                $status->next = '';
                $status->previous = $status_previous->id;
                $status->save();
                $status_previous->next = $status->id;
                $status_previous->save();

                $status_previous = $status;
            }

            $status = new TaskStatus();
            $status->slug = "success";
            $status->title = "Последний статус";
            $status->description = "Статус выставляемый по успешному выполнению задачи";
            $status->task_type = $task_type->id;
            $status->previous = $status_previous->id;
            $status->is_success = true;
            $status->save();
            $status_previous->next = $status->id;
            $status_previous->save();

            $status = new TaskStatus();
            $status->slug = "error";
            $status->title = "Ошибка";
            $status->description = "Статус выставляемый в случае ошибки";
            $status->task_type = $task_type->id;
            $status->is_error = true;
            $status->save();

            if(!isset($data['settings'])) $data['settings'] = [];
            foreach($data['settings'] as $slug => $data_setting){
                self::addSetting($data['task_type']['slug'], $slug, $data_setting['value'], $data_setting['description']);
            }

            if(!isset($data['pause_types'])) $data['pause_types'] = [];
            foreach($data['pause_types'] as $pause_type){
                $pause = new TaskPauseType();
                $pause->task_type = $task_type->id;
                $pause->slug = $pause_type['slug'];
                $pause->title = $pause_type['title'];
                $pause->description = $pause_type['description'];

                $pause->save();
            }

            $transaction->commit();
            echo "Создана задача: ".$data['task_type']['description']."\n";
        }catch (\Exception $e){
            $transaction->rollback();
            echo "\nОткатываем транзакцию\n";
            throw $e;
        }

    }

    /**
     * Метод удаление из базы данных структуры задачи.
     *
     * По слагу находит структуру задачи в базе данных и удаляет её.
     *
     * @param $taskTypeSlug string слаг задачи которую нужно удалить из базы данных
     * @throws \Exception
     * @return bool
     */
    static function rollbackDbEnvironment($taskTypeSlug){
        $taskType = TaskType::find()
            ->with('taskStatuses')
            ->with('taskSettings')
            ->with('taskPauses')
            ->andWhere(['task_type.slug' => $taskTypeSlug])
            ->one();

        if(!isset($taskType))
            return false;

        $tasks = Task::findAll(['type' => $taskType->id]);
        if(sizeof($tasks) > 0){
            if (\Yii::$app->controller->confirm("Для типа задач '{$taskType->title}' есть созданные задачи. Для отката требуется удалить задачи. Удалить тип задачи а вместе с ним и все задачи?")) {
                \Yii::$app->controller->stdout("\n");
                /** @var Task $task */
                foreach ($tasks as $task) {
                    $taskData = TaskData::findOne(['task_id' => $task->id]);
                    if(isset($taskData)){
                        $taskData->delete();
                    }
                    $task->delete();
                    \Yii::$app->controller->stdout("Удалена задача № {$task->id}\n");
                }

                \Yii::$app->controller->stdout("\nЗадачи удалены, продолжаем откат\n", BaseConsole::FG_GREEN);

            }else{
                \Yii::$app->controller->stdout("\nБез удаления задач продолжение невозможно\n", BaseConsole::FG_RED);
                throw new \Exception("Невозможно откатить тип задачи так как существуют ссылающиеся на него задачи");
            }
        }

        foreach($taskType->taskStatuses as $status){
            $status->previous = null;
            $status->next = null;
            $status->save();
        }

        echo "Удаляются статусы\n";
        foreach($taskType->taskStatuses as $status){
            echo "Удаляется статус: $status->description\n";
            $status->delete();
        }
        echo "\n";
        echo "Удаляются настройки\n";
        foreach($taskType->taskSettings as $setting){
            echo "Удаляется настройка: $setting->description\n";
            $setting->delete();
        }

        echo "\n";
        echo "Удаляются типы пауз\n";
        foreach($taskType->taskPauses as $pause){
            echo "Удаляется настройка: $pause->description\n";
            $pause->delete();
        }

        $taskType->delete();

    }

    /**
     * Метод который добавляет к задаче новый статус.
     *
     * В случае когда в процессе разработки изменились условия, или пришла идея оптимизации логики может потребоваться
     * добавить ещё один статус. Данный метод позволяет произвести это действие.
     *
     * @param $taskTypeSlug string слаг типа задачи
     * @param $statusAfterSlug string слаг статуса после которого будет добавляться дополнительный статус
     */
    static function addStatus($taskTypeSlug, $statusAfterSlug, $newStatus = []){

        $taskType = TaskType::findOne(["slug" => $taskTypeSlug]);

        /**
         * Статус после которого нужно вставить новый статус.
         */
        $statusAfter = TaskStatus::findOne(['slug' => $statusAfterSlug]);
        if(!isset($statusAfter)) throw new \Exception("Не найден статус со слагом $statusAfterSlug для задачи со слагом $taskTypeSlug");
        /**
         * Статус перед которым нужно вставить статус.
         */
        $statusBefore = TaskStatus::findOne(['previous' => $statusAfter->next]);


        $status = new TaskStatus();
        $status->slug = $newStatus['slug']."_unqueued";
        $status->title = $newStatus['title'].", НЕ в очереди";
        $status->description = $newStatus['description'].", задача ещё не в очереди";
        $status->task_type = $taskType->id;
        $status->is_unqueued = true;
        $status->waiting = $newStatus['waiting'];
        $status->next = '';
        $status->previous = $status_previous->id;
        $status->save();

        $status_previous->next = $status->id;
        $status_previous->save();

        $status_previous = $status;

        $status = new TaskStatus();
        $status->slug = $newStatus['slug'];
        $status->title = $newStatus['title'];
        $status->description = $newStatus['description'];
        $status->task_type = $taskType->id;
        $status->next = '';
        $status->command = $newStatus['command'];
        $status->previous = $status_previous->id;
        $status->save();
        $status_previous->next = $status->id;
        $status_previous->save();

        $status_previous = $status;

        $status = new TaskStatus();
        $status->slug = $newStatus['slug']."_working";
        $status->title = $newStatus['title'].", ведётся работа";
        $status->description = $newStatus['description'].", задача извлечена из очереди, ведётся работа";
        $status->task_type = $taskType->id;
        $status->next = '';
        $status->previous = $status_previous->id;
        $status->save();
        $status_previous->next = $status->id;
        $status_previous->save();

        $status_previous = $status;

    }

    /**
     * Метод удаляет статус из типа задачи.
     *
     * Условия аналогичные с методом addStatus() но касающиеся удаления статусов.
     *
     * @param $taskTypeSlug слаг типа задачи
     * @param $statusSlug слаг статуса который будет удалён
     */
    static function removeStatus($taskTypeSlug, $statusSlug){

    }

    static function addSetting($taskTypeSlug, $settingSlug, $settingValue, $settingDescription = null)
    {
        $taskType = TaskType::findOne(['slug' => $taskTypeSlug]);
        if(!isset($taskType))
            throw new \Exception("Тип задачи со slug $taskTypeSlug не найден");

        if(!is_string($settingDescription))
            throw new \Exception('Описание настройки должно быть строкой');

        $setting = new TaskSettings();
        $setting->task_type = $taskType->id;
        $setting->slug = $settingSlug;
        $setting->description = $settingDescription;
        if(is_array($settingValue)){
            $setting->value = json_encode($settingValue);
        }else{
            $setting->value = $settingValue;
        }
        $setting->save();
    }
}
