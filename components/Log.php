<?php

namespace chief88\deficit\components;

use chief88\deficit\exceptions\NotStringException;
use chief88\deficit\exceptions\ForbiddenActionException;
use chief88\deficit\exceptions\NoTaskException;
use chief88\deficit\models\TaskLog;
use chief88\deficit\components\DeficitComponent as deficit;
use chief88\deficit\models\TaskType;

/**
 * Class Log
 *
 * @package chief88\deficit\components
 */
class Log
{
    const DEFICIT_CATEGORY = 'deficit_category';

    public static function log2Db($message)
    {
        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;
        $taskLog = new TaskLog();
        $taskLog->task = $deficit->getTask()->id;
        $taskLog->message = json_encode($message);
        $taskLog->save();
    }

    private static function prepareData($message)
    {

        if (!is_array($message))
            throw new ForbiddenActionException('Лог-сообщение должно быть массивом');

        if (array_key_exists('log_queue_name', $message))
            throw new ForbiddenActionException('Ключ log_queue_name зарезервирован как системный, его использование недопустимо');

        if (array_key_exists('task_id', $message))
            throw new ForbiddenActionException('Ключ task_id зарезервирован как системный, его использование недопустимо');

        if (array_key_exists('common', $message))
            throw new ForbiddenActionException('Ключ common зарезервирован как системный, его использование недопустимо');

        if (array_key_exists('timestamp', $message))
            throw new ForbiddenActionException('Ключ timestamp зарезервирован как системный, его использование недопустимо');

        if (array_key_exists('datetime', $message))
            throw new ForbiddenActionException('Ключ datetime зарезервирован как системный, его использование недопустимо');

        foreach ($message as $item => $value) {
            if (!is_string($item))
                throw new NotStringException("Ключ не строка: " . print_r($item, true));

            if (!is_string($value))
                throw new NotStringException("Элемент не строка: " . print_r($value, true));
        }

        /** @var DeficitComponent $deficit */
        $deficit = \Yii::$app->deficitComponent;

        $queue_and_task = [];
        try {
            /*
            * Формируем системные данные, считаем что отправка сообщения производилсь при выполнении какого-то
            * этапа какой-нибудь задачи, поэтому $deficit->getTask() должен нам вернуть эту задачу. Если отправка
            * сообщения производилась без связи с задачей, будет исключение NoTaskException и формирование в коде ниже
            */

            $taskType = $deficit->getTask()->getTaskType();

            $queue_and_task['log_queue_name'] = $taskType->log_group;
            $queue_and_task['task_id'] = $deficit->getTask()->id;
        } catch (NoTaskException $e) {
            /*
            * Если оказалось, что это лог сообщение не привязано к конкретной задаче, сформируем данные указав вместо
            * id задачи 0, и добавив специальный параметр "common" то есть сообщение общее для типа задачи
            */
            $queue_and_task['log_queue_name'] = $deficit->getTaskType()->log_group;
            $queue_and_task['task_id'] = 0;
            $queue_and_task['common'] = true;
        }

        return $queue_and_task;
    }

    public static function info($message)
    {

        $queue_and_task = self::prepareData($message);

        \Yii::info(array_merge($message, $queue_and_task), Log::DEFICIT_CATEGORY);
    }

    public static function warning($message)
    {
        $queue_and_task = self::prepareData($message);

        \Yii::warning(array_merge($message, $queue_and_task), Log::DEFICIT_CATEGORY);
    }

    public static function error($message)
    {

        $queue_and_task = self::prepareData($message);

        \Yii::error(array_merge($message, $queue_and_task), Log::DEFICIT_CATEGORY);
    }
}
