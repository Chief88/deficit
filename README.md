Модуль для работы с очередями
============================

Модуль позволяет асинхронно выполнять задачи и делать это в несколько потоков.

Всё это построено вокруг менеджера очередей и сообщений ActiveMQ

[ActiveMQ Wikipedia](https://ru.wikipedia.org/wiki/Apache_ActiveMQ)

[ActiveMQ официальный сайт](http://activemq.apache.org/)

К действию
============================

1) [Установка](docs/Install.md)

2) [Пример использования в приложении](docs/Using.md)

3) [Тесты](docs/Tests.md)

4) [О версиях](docs/Versions.md)